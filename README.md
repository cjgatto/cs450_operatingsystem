##Purpose: WVU CS450 Operating Systems Design Term Project##
Author: Chris Gatto
Author: Daniel Nutter

Command Line Interface based Operating System project completed for WVU CS 450. The OS implements multiprogramming paradigms including Time Slicing, Context Switching, Process Control Block Priority Queues, and Interrupts. The OS utilizes a Command Line Interface complete with system commands. 