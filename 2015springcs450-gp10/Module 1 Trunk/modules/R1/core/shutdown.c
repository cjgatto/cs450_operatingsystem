#include <modules/R1/commhand.h>
#include <modules/R1/shutdown.h>
#include <core/serial.h>
#include <string.h>

/**
  Processes and handles the shutdown command.
  @return int - signals the continuation of the loop.
*/
int handle_shutdown(void) {

	char input[2];
	serial_println("-murex: Shutdown issued; are you sure? (y/n)");

	while(1) {
		serial_print("Murex: (Shutdown) > ");
		serial_pollln(input, 1);
		serial_println("");

		if(strcmp(input, "n") == 0) {
      return CONTINUE;
    } 
    else if(strcmp(input, "y") == 0) {
    	serial_println("\tEnd Encountered: Shutting Down");
  		return BREAK;
    }
    else {
    	serial_println("\t(Shutdown): Unrecognized entry.");
    }

    strclr(input);
	}

	return CONTINUE;
}

/**********************************************
BEGIN CLI REFACTOR

int handle_shutdown(char *in) {
    if(strcmp(in, "n") == 0) {
      return BREAK;
    } 
    else if(strcmp(in, "y") == 0) {
      serial_println("End Encountered: Shutting Down");
      return KILL;
    }
    else {
      serial_println("Unrecognized entry.");
      return CONTINUE;
    }
}
END CLI REFACTOR
**********************************************/