#include <modules/R1/commhand.h>
#include <modules/R1/version.h>
#include <core/serial.h>
#include <string.h>
#include <system.h>

/**
  Processes and handles the version command.
  @return int - signals the continuation of the loop.
*/
int handle_version(void) {
  char versionDesc[50] = "OS Version: ";
  serial_println(strcat(versionDesc, OS_VERSION));
  serial_println("Module 2 Completion: 27, 2015");
  return CONTINUE;
}