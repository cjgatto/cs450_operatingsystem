#ifndef _STRING_H
#define _STRING_H

#include <system.h>

/**
  Determine if a character is whitespace.
  @param c-character to check
  @return 1 if whitespace 0 if not
*/
int isspace(const char *c);

/**
  Set a region of memory.
  @params s-destination, c-byte to write, n-count
*/
void* memset(void *s, int c, size_t n);

/**
  Copy one string to another location.
  @param char* s1-destination of copy
  @param const char* s2-string to be copied
  @return char*- new string
*/
char* strcpy(char *s1, const char *s2);

/**
  Concatenate the contents of one string onto another.
  @param char* s1-destination
  @param s2-source string
  @return char*- new string
*/
char* strcat(char *s1, const char *s2);

/**
  Returns the length of a string.
  @param const char* s-string to be measured
  @return int-length of the string
*/
int   strlen(const char *s);

/**
  Compares two strings
  @param char* s1- first string to compare
  @param char* s2- second second string to compare
  @return int- 0 if equal,
*/
int   strcmp(const char *s1, const char *s2);

/**
  Procedure..: strtok
  Description..: Split string into tokens
  @Params..: s1-string, s2-delimiter
*/
char* strtok(char *s1, const char *s2);

/**
  Converts an ASCII string to an integer
  @param const char* s-string to be converted
  @return int-the converted integer
*/
int atoi(const char *s);

/**
  Converts an integer base 10 to an ASCII string
  @param int num- number to be converted
  @param char* str-location to store the string
  @return char*-location of new string
*/
char* itoa(int num, char* str);

/**
  Reverses a string
  @param char* str- String to be reversed
*/
void strrev(char* str);

/**
  Turns all lower case letters(a-z) in string to capitals.
  @param char* s1-string to be changed 
*/
void toUpper(char*s1);

/**
  Turns all capital letters(A-Z) in string to lower case.
  @param char* s1-string to be changed
*/
void toLower(char*s1);

/**
  Clears a string by filling it with null characters.
  @param char* str-string to be cleared
*/
void strclr(char*str);

#endif