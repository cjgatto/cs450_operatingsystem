#ifndef _COMMHAND_H
#define _COMMHAND_H

#define BREAK 0
#define CONTINUE 1
#define KILL 2

typedef struct COMMAND {
	char* text;
	int(*funct)(void);
	char* help;
} COMMAND;

#define COMM_COUNT 18
COMMAND commands[COMM_COUNT];
#define SHUTDOWN 0
#define VERSION 1
#define HELP 2
#define GET_DATE 3
#define SET_DATE 4
#define GET_TIME 5
#define SET_TIME 6
// #define SUSPEND 7
// #define RESUME 8 
// #define SET_PRIORITY 9
// #define SHOW_PCB 10
// #define SHOW_ALL 11
// #define SHOW_READY 12
// #define SHOW_BLOCKED 13
// #define CREATE_PCB 14
// #define DELETE_PCB 15
// #define BLOCK 16
// #define UNBLOCK 17

/**
  Prints some basic input upon startup.
*/
void general_info(void);

/**
  Initializes the main program loop, prompting for user input.
*/
void init_commhand(void);

#endif