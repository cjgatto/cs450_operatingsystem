var searchData=
[
  ['make_5fheap',['make_heap',['../heap_8h.html#a686135c02695aef4208f93d4549a15d0',1,'make_heap(u32int base, u32int max, u32int min):&#160;heap.c'],['../heap_8c.html#a686135c02695aef4208f93d4549a15d0',1,'make_heap(u32int base, u32int max, u32int min):&#160;heap.c']]],
  ['max_5fbuff',['MAX_BUFF',['../system_8h.html#a4b70916bdf28ed7afd94e4fb98230a90',1,'system.h']]],
  ['max_5fsize',['max_size',['../structheap.html#aff98f60fdff673e586a88d147da4798c',1,'heap']]],
  ['mem_5fsize',['mem_size',['../paging_8c.html#abf8475f59bfb67fac4b6b5a254dfe56d',1,'paging.c']]],
  ['memset',['memset',['../string_8h.html#ace6ee45c30e71865e6eb635200379db9',1,'memset(void *s, int c, size_t n):&#160;string.c'],['../string_8c.html#ace6ee45c30e71865e6eb635200379db9',1,'memset(void *s, int c, size_t n):&#160;string.c']]],
  ['min',['min',['../structdate__time.html#a3e202b201e6255d975cd6d3aff1f5a4d',1,'date_time']]],
  ['min_5fsize',['min_size',['../structheap.html#af515ec763221e45adce632886c4cb888',1,'heap']]],
  ['minutesloc',['minutesLoc',['../date_8c.html#ac0a0c9b1052d594f302bb8890dd916ea',1,'date.c']]],
  ['module_5fr1',['MODULE_R1',['../mpx__supt_8h.html#a9d88621bfb79cb860b9ea2b5abb1c7f0',1,'mpx_supt.h']]],
  ['module_5fr2',['MODULE_R2',['../mpx__supt_8h.html#a177f46669733cd706e9476485dfd961b',1,'mpx_supt.h']]],
  ['module_5fr3',['MODULE_R3',['../mpx__supt_8h.html#afe0ac8d1ebddd519bed41c8d8e79fad0',1,'mpx_supt.h']]],
  ['module_5fr4',['MODULE_R4',['../mpx__supt_8h.html#a05541487bcf6e840b918f0a6ef097024',1,'mpx_supt.h']]],
  ['module_5fr5',['MODULE_R5',['../mpx__supt_8h.html#a68745bb3b58fd02dcee2cad3b2331def',1,'mpx_supt.h']]],
  ['mon',['mon',['../structdate__time.html#a25b602fa15f03b01f61a900f1f68a67d',1,'date_time']]],
  ['monthloc',['monthLoc',['../date_8c.html#a3633c21cde2662c4055f68fb4fa59ab0',1,'date.c']]],
  ['mpx_5finit',['mpx_init',['../mpx__supt_8c.html#a53332c6a3107a4feed6e2e79690a6ffa',1,'mpx_init(int cur_mod):&#160;mpx_supt.c'],['../mpx__supt_8h.html#a53332c6a3107a4feed6e2e79690a6ffa',1,'mpx_init(int cur_mod):&#160;mpx_supt.c']]],
  ['mpx_5fsupt_2ec',['mpx_supt.c',['../mpx__supt_8c.html',1,'']]],
  ['mpx_5fsupt_2eh',['mpx_supt.h',['../mpx__supt_8h.html',1,'']]]
];
