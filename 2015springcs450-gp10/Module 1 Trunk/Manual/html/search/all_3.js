var searchData=
[
  ['cdir',['cdir',['../paging_8c.html#af7da380833e92d5c7d3c3db41484713b',1,'paging.c']]],
  ['clear_5fbit',['clear_bit',['../paging_8h.html#adcef508c82c20a032508f871e79e1b92',1,'clear_bit(u32int addr):&#160;paging.c'],['../paging_8c.html#adcef508c82c20a032508f871e79e1b92',1,'clear_bit(u32int addr):&#160;paging.c']]],
  ['cli',['cli',['../system_8h.html#a68c330e94fe121eba993e5a5973c3162',1,'system.h']]],
  ['com1',['COM1',['../serial_8h.html#a00dbb3ab1c59e14699be9393693e2248',1,'serial.h']]],
  ['com2',['COM2',['../serial_8h.html#a435e02f194c24c9b0e00d7cd27a1704e',1,'serial.h']]],
  ['com3',['COM3',['../serial_8h.html#abbed02672431595364c5dd35809303a6',1,'serial.h']]],
  ['com4',['COM4',['../serial_8h.html#a595cabb01568ba641574d24546d99c6b',1,'serial.h']]],
  ['commhand_2ec',['commhand.c',['../commhand_8c.html',1,'']]],
  ['commhand_2eh',['commhand.h',['../commhand_8h.html',1,'']]],
  ['continue',['CONTINUE',['../commhand_8h.html#ab711666ad09d7f6c0b91576525ea158e',1,'commhand.h']]],
  ['coprocessor',['coprocessor',['../interrupts_8c.html#a5c538c7b7a55e3c981780b599fcb1de7',1,'interrupts.c']]],
  ['coprocessor_5fsegment',['coprocessor_segment',['../interrupts_8c.html#a1d688a0a370977c03fa98884a6e771e9',1,'interrupts.c']]],
  ['curr_5fheap',['curr_heap',['../heap_8c.html#afaac4d3fb801ecbd3c6fe3c995d5cf82',1,'heap.c']]],
  ['current_5fmodule',['current_module',['../mpx__supt_8c.html#a3d19c725b7f9f45e9da97a79ca6a4737',1,'mpx_supt.c']]]
];
