var searchData=
[
  ['handle_5fgetdate',['handle_getdate',['../date_8h.html#a5efff68d27aa9b4aab81e9181449bd30',1,'handle_getdate():&#160;date.c'],['../date_8c.html#a5efff68d27aa9b4aab81e9181449bd30',1,'handle_getdate():&#160;date.c']]],
  ['handle_5fgettime',['handle_gettime',['../date_8h.html#a737a92d1965e955b18fbca993287dc05',1,'handle_gettime():&#160;date.c'],['../date_8c.html#a737a92d1965e955b18fbca993287dc05',1,'handle_gettime():&#160;date.c']]],
  ['handle_5fhelp',['handle_help',['../help_8h.html#ab05dc9a5eb1dc2f40a35cf1a047cc95a',1,'handle_help(void):&#160;help.c'],['../help_8c.html#ab05dc9a5eb1dc2f40a35cf1a047cc95a',1,'handle_help(void):&#160;help.c']]],
  ['handle_5fsetdate',['handle_setdate',['../date_8h.html#ab3e61a1d3ef65a4770e3c6431b935666',1,'handle_setdate():&#160;date.c'],['../date_8c.html#ab3e61a1d3ef65a4770e3c6431b935666',1,'handle_setdate():&#160;date.c']]],
  ['handle_5fsettime',['handle_settime',['../date_8h.html#a4b3010d04741a2f83fd1c1a5fd380b6f',1,'handle_settime():&#160;date.c'],['../date_8c.html#a4b3010d04741a2f83fd1c1a5fd380b6f',1,'handle_settime():&#160;date.c']]],
  ['handle_5fshutdown',['handle_shutdown',['../shutdown_8h.html#af0522c817919f10cfb100df9599baa17',1,'handle_shutdown(void):&#160;shutdown.c'],['../shutdown_8c.html#af0522c817919f10cfb100df9599baa17',1,'handle_shutdown(void):&#160;shutdown.c']]],
  ['handle_5fversion',['handle_version',['../version_8h.html#aa2b18c86cf6db4e5b5d287f8d1256dcc',1,'handle_version(void):&#160;version.c'],['../version_8c.html#aa2b18c86cf6db4e5b5d287f8d1256dcc',1,'handle_version(void):&#160;version.c']]]
];
