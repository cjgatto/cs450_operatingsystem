var searchData=
[
  ['idle',['idle',['../mpx__supt_8c.html#a83abbeda22fc5e6c2b35523b64199c1c',1,'idle():&#160;mpx_supt.c'],['../mpx__supt_8h.html#a83abbeda22fc5e6c2b35523b64199c1c',1,'idle():&#160;mpx_supt.c']]],
  ['idt_5fset_5fgate',['idt_set_gate',['../tables_8h.html#a9eca3fe1465f8d7d383551d804853139',1,'idt_set_gate(u8int idx, u32int base, u16int sel, u8int flags):&#160;tables.c'],['../tables_8c.html#a9eca3fe1465f8d7d383551d804853139',1,'idt_set_gate(u8int idx, u32int base, u16int sel, u8int flags):&#160;tables.c']]],
  ['init_5fcommhand',['init_commhand',['../commhand_8h.html#a5f6c259a5d805f1a24d35f36cb9207d3',1,'init_commhand(void):&#160;commhand.c'],['../commhand_8c.html#a5f6c259a5d805f1a24d35f36cb9207d3',1,'init_commhand(void):&#160;commhand.c']]],
  ['init_5fgdt',['init_gdt',['../tables_8h.html#a86bb50044169930202cc403376ef40c3',1,'init_gdt():&#160;tables.c'],['../tables_8c.html#a86bb50044169930202cc403376ef40c3',1,'init_gdt():&#160;tables.c']]],
  ['init_5fidt',['init_idt',['../tables_8h.html#a35fe413107af682030ab7a4b6dff19b8',1,'init_idt():&#160;tables.c'],['../tables_8c.html#a35fe413107af682030ab7a4b6dff19b8',1,'init_idt():&#160;tables.c']]],
  ['init_5firq',['init_irq',['../interrupts_8h.html#ad17d9a8aa78440af8fc40d3fd55dbad8',1,'init_irq(void):&#160;interrupts.c'],['../interrupts_8c.html#ad17d9a8aa78440af8fc40d3fd55dbad8',1,'init_irq(void):&#160;interrupts.c']]],
  ['init_5fkheap',['init_kheap',['../heap_8h.html#a755a69ff831b6e23a808dcf4b9944854',1,'heap.h']]],
  ['init_5fpaging',['init_paging',['../paging_8h.html#a919b727f386797a8b9d8eceb5c4e7313',1,'init_paging():&#160;paging.c'],['../paging_8c.html#a919b727f386797a8b9d8eceb5c4e7313',1,'init_paging():&#160;paging.c']]],
  ['init_5fpic',['init_pic',['../interrupts_8h.html#afbc0dbef6f15e2df21b38724ea38c483',1,'init_pic(void):&#160;interrupts.c'],['../interrupts_8c.html#afbc0dbef6f15e2df21b38724ea38c483',1,'init_pic(void):&#160;interrupts.c']]],
  ['init_5fserial',['init_serial',['../serial_8h.html#a7078c07ff8b2c48780558549a8f7cf90',1,'init_serial(int device):&#160;serial.c'],['../serial_8c.html#a7078c07ff8b2c48780558549a8f7cf90',1,'init_serial(int device):&#160;serial.c']]],
  ['init_5fstack',['init_stack',['../stack_8h.html#a0e66ad8844b1da8879303587b2c8fd3c',1,'init_stack(Stack *stack):&#160;stack.c'],['../stack_8c.html#a0e66ad8844b1da8879303587b2c8fd3c',1,'init_stack(Stack *stack):&#160;stack.c']]],
  ['invalid_5fop',['invalid_op',['../interrupts_8c.html#a422d8af60b69cc11dbcbb9d89c69756b',1,'interrupts.c']]],
  ['invalid_5ftss',['invalid_tss',['../interrupts_8c.html#a972be67f6a5432cc8eab8d27755fb00f',1,'interrupts.c']]],
  ['isr0',['isr0',['../interrupts_8c.html#af88eb8e98d943aa0461c01de3cb53493',1,'interrupts.c']]],
  ['isspace',['isspace',['../string_8h.html#a0f3d37d605e9e6d4fc1853ff9d4b91bf',1,'isspace(const char *c):&#160;string.c'],['../string_8c.html#a0f3d37d605e9e6d4fc1853ff9d4b91bf',1,'isspace(const char *c):&#160;string.c']]],
  ['itoa',['itoa',['../string_8h.html#a3281b678762665f64d5e482fb10d1b1f',1,'itoa(int num, char *str):&#160;string.c'],['../string_8c.html#a3281b678762665f64d5e482fb10d1b1f',1,'itoa(int num, char *str):&#160;string.c']]],
  ['itobcd',['iToBCD',['../date_8h.html#ae2261bee46d0ca09e63bc02db90dce20',1,'iToBCD(int num, unsigned char *byte):&#160;date.c'],['../date_8c.html#ae2261bee46d0ca09e63bc02db90dce20',1,'iToBCD(int num, unsigned char *byte):&#160;date.c']]]
];
