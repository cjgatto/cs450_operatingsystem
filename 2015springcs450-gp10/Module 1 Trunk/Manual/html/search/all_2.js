var searchData=
[
  ['backspace',['BACKSPACE',['../serial_8h.html#a629568514359445d2fbda71d70eeb1ce',1,'serial.h']]],
  ['backspace_5fsequence',['backspace_sequence',['../serial_8h.html#a762d85d93503301fdbd487e318279a03',1,'backspace_sequence(void):&#160;serial.c'],['../serial_8c.html#a762d85d93503301fdbd487e318279a03',1,'backspace_sequence(void):&#160;serial.c']]],
  ['base',['base',['../structidt__struct.html#ab5763c2b18c825c8b8fba44b2e60ddc1',1,'idt_struct::base()'],['../structgdt__descriptor__struct.html#ab5763c2b18c825c8b8fba44b2e60ddc1',1,'gdt_descriptor_struct::base()'],['../structheap.html#ab5763c2b18c825c8b8fba44b2e60ddc1',1,'heap::base()'],['../tables_8h.html#ab5763c2b18c825c8b8fba44b2e60ddc1',1,'base():&#160;tables.h']]],
  ['base_5fhigh',['base_high',['../structidt__entry__struct.html#aa5444beb10d8cdc1d75a18d338f1b3ea',1,'idt_entry_struct::base_high()'],['../structgdt__entry__struct.html#a706c81b840522a69ab6e6e941630d5e4',1,'gdt_entry_struct::base_high()'],['../tables_8h.html#a706c81b840522a69ab6e6e941630d5e4',1,'base_high():&#160;tables.h']]],
  ['base_5flow',['base_low',['../structidt__entry__struct.html#a0a776dced2c26f16298425cde39f8364',1,'idt_entry_struct::base_low()'],['../structgdt__entry__struct.html#a0a776dced2c26f16298425cde39f8364',1,'gdt_entry_struct::base_low()'],['../tables_8h.html#a0a776dced2c26f16298425cde39f8364',1,'base_low():&#160;tables.h']]],
  ['base_5fmid',['base_mid',['../structgdt__entry__struct.html#a35c709a004babd09046db9f667ba0646',1,'gdt_entry_struct::base_mid()'],['../tables_8h.html#a35c709a004babd09046db9f667ba0646',1,'base_mid():&#160;tables.h']]],
  ['bcdtoi',['bcdToI',['../date_8h.html#af7a731ecf355d809adb55a866e2c2711',1,'bcdToI(unsigned char byte):&#160;date.c'],['../date_8c.html#af7a731ecf355d809adb55a866e2c2711',1,'bcdToI(unsigned char byte):&#160;date.c']]],
  ['block',['block',['../structindex__entry.html#a149bb6ecda94461da44658b57b575133',1,'index_entry']]],
  ['bounds',['bounds',['../interrupts_8c.html#a96724d36ad85f9a02231d8e6fe0434df',1,'interrupts.c']]],
  ['break',['BREAK',['../commhand_8h.html#abe022c8f09db1f0680a92293523f25dd',1,'commhand.h']]],
  ['breakpoint',['breakpoint',['../interrupts_8c.html#a874043e2396dd8ce20ec7af3ea1e2a86',1,'interrupts.c']]]
];
