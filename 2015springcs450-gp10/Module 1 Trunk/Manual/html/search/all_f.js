var searchData=
[
  ['page_5fdir',['page_dir',['../structpage__dir.html',1,'']]],
  ['page_5fentry',['page_entry',['../structpage__entry.html',1,'']]],
  ['page_5ffault',['page_fault',['../interrupts_8c.html#ab3800dd0177baaef4da00494edfb32d9',1,'interrupts.c']]],
  ['page_5fsize',['PAGE_SIZE',['../paging_8h.html#a7d467c1d283fdfa1f2081ba1e0d01b6e',1,'PAGE_SIZE():&#160;paging.h'],['../paging_8c.html#a1cc472551ec40b65eef931fde01054e7',1,'page_size():&#160;paging.c']]],
  ['page_5ftable',['page_table',['../structpage__table.html',1,'']]],
  ['pages',['pages',['../structpage__table.html#a5cca43bbc67fbbb87eedf886c6e32efb',1,'page_table']]],
  ['paging_2ec',['paging.c',['../paging_8c.html',1,'']]],
  ['paging_2eh',['paging.h',['../paging_8h.html',1,'']]],
  ['param',['param',['../structparam.html',1,'']]],
  ['params',['params',['../mpx__supt_8c.html#a3b4b77494d0fad58939896ddc5290c99',1,'mpx_supt.c']]],
  ['phys_5falloc_5faddr',['phys_alloc_addr',['../heap_8c.html#a6dfa4ef84e115e891b3679e4932b5c49',1,'phys_alloc_addr():&#160;heap.c'],['../paging_8c.html#a6dfa4ef84e115e891b3679e4932b5c49',1,'phys_alloc_addr():&#160;heap.c']]],
  ['pic1',['PIC1',['../interrupts_8c.html#a6b115109e4a0d3c5fb6252d091e86bfe',1,'interrupts.c']]],
  ['pic2',['PIC2',['../interrupts_8c.html#ac90003f52c8d736193efc954ece08e58',1,'interrupts.c']]],
  ['pop_5fstack',['pop_stack',['../stack_8h.html#ae22d1180646b883de8de2b1af4606cac',1,'pop_stack(Stack *stack):&#160;stack.c'],['../stack_8c.html#ae22d1180646b883de8de2b1af4606cac',1,'pop_stack(Stack *stack):&#160;stack.c']]],
  ['power',['power',['../date_8h.html#aee4072f52ae76435a626b818498e3e27',1,'power(int base, int exponent):&#160;date.c'],['../date_8c.html#aee4072f52ae76435a626b818498e3e27',1,'power(int base, int exponent):&#160;date.c']]],
  ['present',['present',['../structpage__entry.html#a69718d61bbe7faf204d90744c9824c52',1,'page_entry']]],
  ['prompt',['PROMPT',['../system_8h.html#accdbea14ea06c15e271784368bd993e8',1,'system.h']]],
  ['push_5fstack',['push_stack',['../stack_8h.html#a59eab65b16cba77f4bf2c0719947b879',1,'push_stack(Stack *stack, char *command):&#160;stack.c'],['../stack_8c.html#a59eab65b16cba77f4bf2c0719947b879',1,'push_stack(Stack *stack, char *command):&#160;stack.c']]]
];
