#include <core/io.h>
#include <core/serial.h>
#include <string.h>
#include <system.h>
#include <modules/R1/date.h>

unsigned char getSeconds();
unsigned char getHours();
unsigned char getDayInWeek();
unsigned char getDayInMonth();
unsigned char getMonth();
unsigned char getYear();
int power(int base,int exponent);
int bcdToI(unsigned char byte);
void iToBCD(int num,unsigned char* byte);
void setMinutes(int num);
unsigned char getMinutes();
void setSeconds(int num);
void setHours(int num);
void setDayInWeek(int num);
void setDayInMonth(int num);
void setMonth(int num);
void setYear(int num);
void setTime(int hr,int min, int sec);
void setDate(int month, int day, int year);
void getTime();
void getDate();

unsigned const char secondsLoc=0x00;
unsigned const char minutesLoc=0x02;
unsigned const char hoursLoc=0x04;
unsigned const char dayInWeekLoc=0x06;
unsigned const char dayInMonthLoc=0x07;
unsigned const char monthLoc=0x08;
unsigned const char yearLoc=0x09;
char output[MAX_BUFF];
int handle_getdate();
int handle_setdate();
int handle_gettime();
int handle_settime();

/**
	handles the getdate command from commhand
*/
int handle_getdate(){
	getDate();
	serial_println(output);
	return CONTINUE;
}
/**
	handles the setdate command from commhand and seperates input
*/
int handle_setdate(){
	int month,day,year;
	month=atoi(strtok(NULL, "/"));
	day=atoi(strtok(NULL, "/"));
	year=atoi(strtok(NULL, "/"));
	setDate(month,day,year);
	handle_getdate();
	return CONTINUE;
}

/**
	handles the gettime command from commhand
*/
int handle_gettime(){
	getTime();
	serial_println(output);
	return CONTINUE;
}

/**
	handles the settime command from commhand and seperates input
*/
int handle_settime(){
	int hr,min,sec;
	hr=atoi(strtok(NULL, ":"));
	min=atoi(strtok(NULL, ":"));
	sec=atoi(strtok(NULL, ":"));
	setTime(hr,min,sec);
	handle_gettime();
	return CONTINUE;
}

/**
	Checks for valid time and sets the clock to that time
	@param int hr-new hour 
	@param int min-new minute
	@param int sec-new second
*/
void setTime(int hr,int min, int sec){
	if(hr>=24||hr<0||min<0||min>=60||sec<0||sec>60){
		serial_println("Invalid time entered");
		return;
	}
	cli();
	setHours(hr);
	setMinutes(min);
	setSeconds(sec);
	sti();
}

/**
	Checks for valid input and sets the clock to that date
	@param int month-new month of the year
	@param int day- new day of the month
	@param int year- new year
*/
void setDate(int month, int day, int year){
	if(month<1||month>12||year<0||year>99||day<1||day>31){
		serial_println("Invalid date entered");
		return;
	}
	cli();
	//setDayInWeek(wDay);
	setMonth(month);
	setDayInMonth(day);
	setYear(year);
	sti();
}
/**
	Gets the time from the clock and stores it as a string into the output buffer
*/
void getTime(){
	int store;
	strclr(output);
	char temp2[MAX_BUFF];
	strclr(temp2);
	char* temp="Time is ";
	strcat(output,temp);
	temp=":";
	store=bcdToI(getHours());
	if(store>=10)itoa(store,temp2);
	else{
		temp2[0]='0';
		temp2[1]=(store&0xff)+'0';
		temp2[3]='\0';
	}
	strcat(output,temp2);
	strcat(output,temp);
	store=bcdToI(getMinutes());
	if(store>=10)itoa(store,temp2);
	else{
		temp2[0]='0';
		temp2[1]=(store&0xff)+'0';
		temp2[3]='\0';
	}
	strcat(output,temp2);
	strcat(output,temp);
	store=bcdToI(getSeconds());
	if(store>=10)itoa(store,temp2);
	else{
		temp2[0]='0';
		temp2[1]=(store&0xff)+'0';
		temp2[3]='\0';
	}
	strcat(output,temp2);
}

/**
	Gets the Date from the clock and stores it as a string into the output buffer
*/
void getDate(){
	int store;
	strclr(output);
	char temp2[MAX_BUFF];
	strclr(temp2);
	*temp2='\0';
	char* temp="Date is ";
	strcat(output,temp);
	temp="/";
	itoa(bcdToI(getMonth()),temp2);
	strcat(output,temp2);
	strcat(output,temp);
	itoa(bcdToI(getDayInMonth()),temp2);
	strcat(output,temp2);
	strcat(output,temp);
	store=bcdToI(getYear());
	if(store>=10)itoa(store,temp2);
	else{
		temp2[0]='0';
		temp2[1]=(store&0xff)+'0';
		temp2[3]='\0';
	}
	strcat(output,temp2);
}

unsigned char getSeconds(){
	outb(0x70,secondsLoc);
	unsigned char temp=(unsigned char) inb(0x71);
	return temp;
}

unsigned char getMinutes(){
	outb(0x70,minutesLoc);
	return (unsigned char)inb(0x71);
}

unsigned char getHours(){
	outb(0x70,hoursLoc);
	return (unsigned char)inb(0x71);
}

unsigned char getDayInWeek(){
	outb(0x70,dayInWeekLoc);
	return (unsigned char)inb(0x71);
}

unsigned char getDayInMonth(){
	outb(0x70,dayInMonthLoc);
	return (unsigned char)inb(0x71);
}

unsigned char getMonth(){
	outb(0x70,monthLoc);
	return (unsigned char)inb(0x71);
}

unsigned char getYear(){
	outb(0x70,yearLoc);
	return (unsigned char)inb(0x71);
}


/**
  Converts integer to BCD in unsigned char array.
  @param int num-the number to be put into BCD
  @param unsigned char* byte-The unsigned char array destination
*/
void iToBCD(int num,unsigned char* byte){
	int i=1;
	while(power(10,i)<num){
		i++;
	}
	if(i&0x01){}
	else{
	i--;
	}
	while(i>0){
		int i1=num/power(10,i);
		num-=i1*power(10,i--);
		int i2=num/power(10,i);
		num-=i2*power(10,i--);
		*byte++=(((i1&0xff)<<4)|(i2&0xff));
	}
}
/**
  Converts BCD byte to an int.
  @param unsigned char byte-The BCD byte that is to be turned into an int
  @return int- the converted integer
*/
int bcdToI(unsigned char byte){	
	int i=(int)((byte&0xf0)>>4);
	i*=10;	
	i+=(int)(byte&0x0f);	
	return i;
}

/**
  Returns base to the exponent
  @param int base-base number
  @param int exponent-exponent number must be non negative.
*/
int power(int base,int exponent){
	int total=1;	
	while(exponent>0){
	total*=base;
	exponent--;
	}
	return total;
}

void setSeconds(int num){
	outb(0x70,secondsLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}

void setMinutes(int num){
	outb(0x70,minutesLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}

void setHours(int num){
	outb(0x70,hoursLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}

void setDayInWeek(int num){
	outb(0x70,dayInWeekLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}

void setDayInMonth(int num){
	outb(0x70,dayInMonthLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}

void setMonth(int num){
	outb(0x70,monthLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}

void setYear(int num){
	outb(0x70,yearLoc);
	unsigned char temp[1];
	iToBCD(num,temp);
	outb(0x71,temp[0]);
}
