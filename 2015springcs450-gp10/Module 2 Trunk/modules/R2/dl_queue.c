// /*
// 	Linked List Blocked Queue
// 	FIFO
// */

// #include <core/serial.h>
// #include <modules/R2/pcb.h>
// #include <modules/R2/dl_queue.h>
// #include <string.h>

// // REMOVE ME
// #include <core/serial.h>
	
// QUEUE readyQueue = {.head = NULL, .tail = NULL, .count = 0};
// QUEUE blockedQueue = {.head = NULL, .tail = NULL, .count = 0};

// void insertPCB(PCB* pcb) {
// 	if(pcb->rrb == READY || pcb->rrb == RUNNING) { //place into readyQueue
// 		if(readyQueue.head == NULL) { // If the queue is empty
// 			readyQueue.head = readyQueue.tail = pcb;
// 			pcb->next = NULL;
// 			pcb->prev = NULL;
// 		}
// 		else { // The queue is not empty, add it as tail
// 			PCB* oldTail = readyQueue.tail;
// 			oldTail->next = pcb;
// 			pcb->prev = oldTail;
// 			pcb->next = NULL;
// 			readyQueue.tail = pcb;
// 		}
// 		readyQueue.count++;
// 	}
// 	else if(pcb->rrb == BLOCKED) { //place into blockedQueue
// 		if(blockedQueue.head == NULL) { // If the queue is empty
// 			blockedQueue.head = blockedQueue.tail = pcb;
// 			pcb->next = NULL;
// 			pcb->prev = NULL;
// 		}
// 		else { // The queue is not empty, add it as tail
// 			PCB* oldTail = blockedQueue.tail;
// 			oldTail->next = pcb;
// 			pcb->prev = oldTail;
// 			pcb->next = NULL;
// 			blockedQueue.tail = pcb;
// 		}
// 		blockedQueue.count++;
// 	}
// 	else { //The state is unkown
// 		return;
// 	}
// 	// printQueues();
// }

// int removePCB(PCB* pcb){
// 	if(pcb->rrb == READY || pcb->rrb == RUNNING) { // remove from readyQueue
// 		if(readyQueue.head == pcb && readyQueue.tail == pcb) {
// 			readyQueue.head = readyQueue.tail = NULL;
// 		}
// 		else if(readyQueue.head == pcb) {
// 			readyQueue.head = pcb->next;
// 			readyQueue.head->prev = NULL;
// 		}
// 		else if(readyQueue.tail == pcb) {
// 			readyQueue.tail = pcb->prev;
// 			readyQueue.tail->next = NULL;
// 		}
// 		else {
// 			pcb->next->prev = pcb->prev;
// 			pcb->prev->next = pcb->next;
// 		}
// 		readyQueue.count--;
// 	}
// 	else if(pcb->rrb == BLOCKED) { // remove from blocked
// 		if(blockedQueue.head == pcb && blockedQueue.tail == pcb) {
// 			blockedQueue.head = blockedQueue.tail = NULL;
// 		}
// 		else if(blockedQueue.head == pcb) {
// 			blockedQueue.head = pcb->next;
// 			blockedQueue.head->prev = NULL;
// 		}
// 		else if(blockedQueue.tail == pcb) {
// 			blockedQueue.tail = pcb->prev;
// 			blockedQueue.tail->next = NULL;
// 		}
// 		else {
// 			pcb->next->prev = pcb->prev;
// 			pcb->prev->next = pcb->next;
// 		}
// 		blockedQueue.count--;
// 	}
// 	else { // The state is unkown
// 		return ERROR;
// 	}
// 	// printQueues();
// 	return CONTINUE;
// }

// PCB* findPCB(char name[16]) {
// 	PCB* curr=readyQueue.head; // search ready queue first
// 	while(curr){
// 		if(strcmp(name,curr->name)==0) {
// 			return curr;
// 		}
// 		curr=curr->next;
// 	}

// 	curr=blockedQueue.head; // search blocked queue second
// 	while(curr){
// 		if(strcmp(name,curr->name)==0) {
// 			return curr;
// 		}
// 		curr=curr->next;
// 	}
// 	return NULL;
// }

// void printQueues(void) {
// 	serial_println("Ready Queue: ");
// 	PCB* pcb = readyQueue.head;
// 	while(pcb) {
// 		char temp1[5] = "";
// 		serial_print("\tPCB Name: ");
// 		serial_println(pcb->name);
// 		strclr(temp1);

// 		serial_print("\tClass: ");
// 		itoa(pcb->clas, temp1);
// 		serial_println(temp1);
// 		strclr(temp1);

// 		serial_print("\tPrio: ");
// 		itoa(pcb->clas, temp1);
// 		serial_println(temp1);
// 		strclr(temp1);

// 		serial_print("\trrb: ");
// 		itoa(pcb->clas, temp1);
// 		serial_println(temp1);
// 		strclr(temp1);

// 		serial_print("\tsus: ");
// 		itoa(pcb->clas, temp1);
// 		serial_println(temp1);
// 		strclr(temp1);

// 		pcb = pcb->next;
// 	}
// 	serial_println("Blocked Queue:");
// 	pcb = blockedQueue.head;
// 	while(pcb) {
// 		char temp2[5] = "";
// 		serial_print("\tPCB Name: ");
// 		serial_println(pcb->name);
// 		strclr(temp2);

// 		serial_print("\tClass: ");
// 		itoa(pcb->clas, temp2);
// 		serial_println(temp2);
// 		strclr(temp2);

// 		serial_print("\tPrio: ");
// 		itoa(pcb->clas, temp2);
// 		serial_println(temp2);
// 		strclr(temp2);

// 		serial_print("\trrb: ");
// 		itoa(pcb->clas, temp2);
// 		serial_println(temp2);
// 		strclr(temp2);

// 		serial_print("\tsus: ");
// 		itoa(pcb->clas, temp2);
// 		serial_println(temp2);
// 		strclr(temp2);
		
// 		pcb = pcb->next;
// 	}
// }

// // insertPCB	
// 	// if(pcb->rrb == 3) {
// 	// 	if(blockedQueue.head){
// 	// 		pcb->prev=blockedQueue.tail;
// 	// 		blockedQueue.tail->next=pcb;
// 	// 		blockedQueue.tail=pcb;
// 	// 	}else{
// 	// 		blockedQueue.head=blockedQueue.tail=pcb;
// 	// 	}
// 	// }else{
// 	// 	if(readyQueue.head){
// 	// 		PCB* curr = readyQueue.head;
// 	// 		while(curr->priority > pcb->priority) {
// 	// 			curr = curr->next;
// 	// 			if(curr=='\0')break;
// 	// 		}
// 	// 		if(readyQueue.head==curr){
// 	// 			pcb->next=curr;
// 	// 			curr->prev=pcb;
// 	// 			readyQueue.head=pcb;
// 	// 		}else if(curr=='\0'){
// 	// 			readyQueue.tail->next=pcb;
// 	// 			pcb->prev=readyQueue.tail;
// 	// 			readyQueue.tail=pcb;
// 	// 		}else{
// 	// 			pcb->prev=curr->prev;
// 	// 			pcb->prev->next=pcb;
// 	// 			pcb->next=curr;
// 	// 			curr->prev=pcb;
// 	// 		}
// 	// 	}else{
// 	// 		readyQueue.head=readyQueue.tail=pcb;
// 	// 	}
// 	// }

// // removePCB
// 	// if(pcb){
// 	// 	if(blockedQueue.head==pcb)blockedQueue.head=pcb->next;
// 	// 	if(blockedQueue.tail==pcb)blockedQueue.tail=pcb->prev;
// 	// 	if(readyQueue.head==pcb)readyQueue.head=pcb->next;
// 	// 	if(readyQueue.tail==pcb)readyQueue.tail=pcb->prev;
// 	// 	if(pcb->next)pcb->next->prev=pcb->prev;
// 	// 	if(pcb->prev)pcb->prev->next=pcb->next;
// 	// 	pcb->prev=pcb->next='\0';
// 	// 	return 1;
// 	// }return 0;