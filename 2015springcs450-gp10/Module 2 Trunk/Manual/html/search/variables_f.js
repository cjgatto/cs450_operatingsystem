var searchData=
[
  ['page_5fsize',['page_size',['../paging_8c.html#a1cc472551ec40b65eef931fde01054e7',1,'paging.c']]],
  ['pages',['pages',['../structpage__table.html#a5cca43bbc67fbbb87eedf886c6e32efb',1,'page_table']]],
  ['params',['params',['../mpx__supt_8c.html#a3b4b77494d0fad58939896ddc5290c99',1,'mpx_supt.c']]],
  ['phys_5falloc_5faddr',['phys_alloc_addr',['../heap_8c.html#a6dfa4ef84e115e891b3679e4932b5c49',1,'phys_alloc_addr():&#160;heap.c'],['../paging_8c.html#a6dfa4ef84e115e891b3679e4932b5c49',1,'phys_alloc_addr():&#160;heap.c']]],
  ['present',['present',['../structpage__entry.html#a69718d61bbe7faf204d90744c9824c52',1,'page_entry']]],
  ['prev',['prev',['../struct_p_c_b.html#a1a5941945be5a1afe7a4ce744eb1c739',1,'PCB']]],
  ['priority',['priority',['../struct_p_c_b.html#acec9ce2df15222151ad66fcb1d74eb9f',1,'PCB']]]
];
