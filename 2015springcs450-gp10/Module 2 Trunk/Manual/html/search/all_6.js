var searchData=
[
  ['find_5ffree',['find_free',['../paging_8c.html#abe201f9294ab23125146fff36fe95187',1,'paging.c']]],
  ['findpcb',['findPCB',['../pcb_8h.html#a7ba267136f57cf42bd682162b835a2b2',1,'findPCB(char *name):&#160;pcb.c'],['../pcb_8c.html#a7ba267136f57cf42bd682162b835a2b2',1,'findPCB(char *name):&#160;pcb.c']]],
  ['first_5ffree',['first_free',['../paging_8h.html#acb3c25257061521382c7ba900c1c1ab4',1,'paging.h']]],
  ['flags',['flags',['../structidt__entry__struct.html#a138dda98fcd4738346af61bcca8cf4b4',1,'idt_entry_struct::flags()'],['../structgdt__entry__struct.html#a138dda98fcd4738346af61bcca8cf4b4',1,'gdt_entry_struct::flags()'],['../tables_8h.html#a138dda98fcd4738346af61bcca8cf4b4',1,'flags():&#160;tables.h']]],
  ['footer',['footer',['../structfooter.html',1,'']]],
  ['frameaddr',['frameaddr',['../structpage__entry.html#a95b631ccb680a6af3d4d4a4fda3ca440',1,'page_entry']]],
  ['frames',['frames',['../paging_8c.html#a76492529572a1a20a06076ac40d66b29',1,'paging.c']]],
  ['freepcb',['freePCB',['../pcb_8h.html#ae5d11c048f8685542dada51c0823ab2f',1,'freePCB(PCB *pcb):&#160;pcb.c'],['../pcb_8c.html#ae5d11c048f8685542dada51c0823ab2f',1,'freePCB(PCB *pcb):&#160;pcb.c']]],
  ['funct',['funct',['../struct_c_o_m_m_a_n_d.html#a1a476022ae47ade517f211a75ed1be7b',1,'COMMAND']]]
];
