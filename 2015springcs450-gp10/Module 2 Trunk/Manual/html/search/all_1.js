var searchData=
[
  ['access',['access',['../structgdt__entry__struct.html#a360a726ac0b61d9e4e1be3ad34f80244',1,'gdt_entry_struct::access()'],['../tables_8h.html#a360a726ac0b61d9e4e1be3ad34f80244',1,'access():&#160;tables.h']]],
  ['accessed',['accessed',['../structpage__entry.html#afb99a0327fa4c7332208a4c69586c8ec',1,'page_entry']]],
  ['advance_5flocation',['advance_location',['../serial_8h.html#a9cd084d6adf11ea33ce57560db8d1702',1,'advance_location(void):&#160;serial.c'],['../serial_8c.html#a9cd084d6adf11ea33ce57560db8d1702',1,'advance_location(void):&#160;serial.c']]],
  ['alloc',['alloc',['../heap_8h.html#a2b1d5a9ba11695605f74fc10cd719af5',1,'alloc(u32int size, heap *hp, int align):&#160;heap.c'],['../heap_8c.html#a06dae34c7e7c73d518de00212a7c92da',1,'alloc(u32int size, heap *h, int align):&#160;heap.c']]],
  ['allocatepcb',['allocatePCB',['../pcb_8h.html#a08e14856b0c743266c38c7aabc687c97',1,'allocatePCB():&#160;pcb.c'],['../pcb_8c.html#a08e14856b0c743266c38c7aabc687c97',1,'allocatePCB():&#160;pcb.c']]],
  ['application',['APPLICATION',['../pcb_8h.html#a796bd7c6ba2e59281760fb155c6287e8',1,'pcb.h']]],
  ['asm',['asm',['../system_8h.html#a71921cebf4610b0dbb2b7a0daaf3fedf',1,'system.h']]],
  ['asm_2eh',['asm.h',['../asm_8h.html',1,'']]],
  ['atoi',['atoi',['../string_8h.html#a30670a60464f77af17dfb353353d6df8',1,'atoi(const char *s):&#160;string.c'],['../string_8c.html#a30670a60464f77af17dfb353353d6df8',1,'atoi(const char *s):&#160;string.c']]]
];
