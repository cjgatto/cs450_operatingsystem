var searchData=
[
  ['read',['READ',['../mpx__supt_8h.html#ada74e7db007a68e763f20c17f2985356',1,'mpx_supt.h']]],
  ['ready',['READY',['../pcb_8h.html#ad1235d5ce36f7267285e82dccd428aa6',1,'pcb.h']]],
  ['readyqueue',['readyQueue',['../pcb_8c.html#a462a7af2d1c38bc33f16c243f110e090',1,'pcb.c']]],
  ['removepcb',['removePCB',['../pcb_8h.html#a3a96f518e7e034c3bc917ba2f0efbcd2',1,'removePCB(PCB *pcb):&#160;pcb.c'],['../pcb_8c.html#a3a96f518e7e034c3bc917ba2f0efbcd2',1,'removePCB(PCB *pcb):&#160;pcb.c']]],
  ['repr_5fclas',['repr_clas',['../pcb_8h.html#accf6a8e08f7a8236a8802ea9b1486d33',1,'repr_clas(int clas):&#160;pcb.c'],['../pcb_8c.html#accf6a8e08f7a8236a8802ea9b1486d33',1,'repr_clas(int clas):&#160;pcb.c']]],
  ['repr_5fstate',['repr_state',['../pcb_8h.html#ac077b05cbcefd172adbd5d43877a024e',1,'repr_state(int rrb):&#160;pcb.c'],['../pcb_8c.html#ac077b05cbcefd172adbd5d43877a024e',1,'repr_state(int rrb):&#160;pcb.c']]],
  ['repr_5fstatus',['repr_status',['../pcb_8h.html#abd2b790fdde69c100bb8f330e9f64b39',1,'repr_status(int sus):&#160;pcb.c'],['../pcb_8c.html#abd2b790fdde69c100bb8f330e9f64b39',1,'repr_status(int sus):&#160;pcb.c']]],
  ['reserved',['reserved',['../structpage__entry.html#a428bdea224227681cbba9cb45f3cca62',1,'page_entry::reserved()'],['../interrupts_8c.html#ad686e3fee8ec8346a6d8e98d970a02dd',1,'reserved():&#160;interrupts.c']]],
  ['resume',['RESUME',['../commhand_8h.html#a58ed6a8ccad6ef42dc18ad5cfe848256',1,'commhand.h']]],
  ['rrb',['rrb',['../struct_p_c_b.html#a73347a9a096f8bc81ba2c27a3285aed0',1,'PCB']]],
  ['rtc_5fisr',['rtc_isr',['../interrupts_8c.html#a52f2615cebbdeab188085a03c913fcf9',1,'interrupts.c']]],
  ['running',['RUNNING',['../pcb_8h.html#a6fb7181d994ee98e735494be55809708',1,'pcb.h']]]
];
