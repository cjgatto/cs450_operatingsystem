var searchData=
[
  ['name',['name',['../struct_p_c_b.html#a8978faffdab7f0fea388a112afd39a5e',1,'PCB']]],
  ['new_5fframe',['new_frame',['../paging_8h.html#a04bce9da2c1d7c59f6efd8e4d9b54db7',1,'new_frame(page_entry *page):&#160;paging.c'],['../paging_8c.html#a04bce9da2c1d7c59f6efd8e4d9b54db7',1,'new_frame(page_entry *page):&#160;paging.c']]],
  ['next',['next',['../struct_p_c_b.html#a8d0f9826cd81ae383f5ccbcecd0e0db4',1,'PCB']]],
  ['nframes',['nframes',['../paging_8c.html#abf36580d5618820f15388083c9313e60',1,'paging.c']]],
  ['nmi',['nmi',['../interrupts_8c.html#aa42d18df06cf68f8c05fded5344c4c7e',1,'interrupts.c']]],
  ['no_5ferror',['NO_ERROR',['../serial_8c.html#a258bb72419ef143530a2f8f55e7d57af',1,'serial.c']]],
  ['no_5fwarn',['no_warn',['../system_8h.html#ab3bb695e7817363c7bdb781f214e83a2',1,'system.h']]],
  ['nop',['nop',['../system_8h.html#a6c92c29fa8e83ab85e05543010e10d7c',1,'system.h']]],
  ['not_5fsuspended',['NOT_SUSPENDED',['../pcb_8h.html#a5add4e408a95d8a4a34ecc685a060b2d',1,'pcb.h']]],
  ['null',['NULL',['../system_8h.html#a070d2ce7b6bb7e5c05602aa8c308d0c4',1,'system.h']]]
];
