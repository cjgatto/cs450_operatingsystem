#ifndef _SERIAL_H
#define _SERIAL_H

#define COM1 0x3f8
#define COM2 0x2f8
#define COM3 0x3e8
#define COM4 0x2e8

#define ESCAPE '\33'
#define BACKSPACE '\177'
#define TAB '\t'

//Command History
#define HISTORY_LIMIT 50

/**
  Initializes the serial port.
  @params device - Port to connect to
*/
int init_serial(int device);

/**
  Prints a message to output, followed by a new line.
  @params msg - The text to be printed
  @return int - Successful or not
*/
int serial_println(const char *msg);

/**
  Prints a message to output.
  @params msg - The text to be printed
  @return int - Successful or not
*/
int serial_print(const char *msg);

/**
  Serial_polln handles all keyboard input. The gathered string is stored in the 'line' parameter.
  @params line[] - The string to store user input
  @params MAX_LENGTH - Maximum number of input characters
*/
void serial_pollln(char line[], int len);

/**
  Sets serial_port_out to the given device address.
  All serial output, such as that from serial_println, will be
  directed to this device. 
  @params device - desired device
  @returns int - Successful or not
*/
int set_serial_out(int device);

/**
  Sets serial_port_in to the given device address.
  All serial input, such as console input via a virtual machine,
  QEMU/Bochs/etc, will be directed to this device. 
  @params device - desired device
  @returns int - Successful or not
*/
int set_serial_in(int device);

/**
  Executes one full backspace sequence to the ouput. 
*/
void backspace_sequence(void);

/**
  Supplies the next location index when searching through input history. 
  @returns int - The next usable index.
*/
int get_prev_location(void);

int advance_location(void);

/**
  Supplies the next location index when storing input history. 
  @returns int - The next usable index.
*/
int get_next_location(void);

#endif
