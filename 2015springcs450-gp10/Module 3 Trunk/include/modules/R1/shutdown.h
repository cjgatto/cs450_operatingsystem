#ifndef _SHUTDOWN_H
#define _SHUTDOWN_H

/**
  Processes and handles the shutdown command.
  @return int - signals the continuation of the loop.
*/
int handle_shutdown(void);

#endif