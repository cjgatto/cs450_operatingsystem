#include <modules/R1/commhand.h>
#include <modules/R1/shutdown.h>
#include <core/serial.h>
#include <string.h>
#include <modules/R2/pcb.h>

/**
  Processes and handles the shutdown command.
  @return int - signals the continuation of the loop.
*/
int handle_shutdown(void) {

	char input[2];
	serial_println("-murex: Shutdown issued; are you sure? (y/n)");

	while(1) {
		serial_print("Murex: (Shutdown) > ");
		serial_pollln(input, 1);
		serial_println("");

		if(strcmp(input, "n") == 0) {
      return CONTINUE;
    } 
    else if(strcmp(input, "y") == 0) {
    	serial_println("\tEnd Encountered: Shutting Down");
      handle_clearqueues();
  		return BREAK;
    }
    else {
    	serial_println("\t(Shutdown): Unrecognized entry.");
    }

    strclr(input);
	}

	return CONTINUE;
}