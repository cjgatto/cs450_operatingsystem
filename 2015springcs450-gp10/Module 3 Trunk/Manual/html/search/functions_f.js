var searchData=
[
  ['removepcb',['removePCB',['../pcb_8h.html#a3a96f518e7e034c3bc917ba2f0efbcd2',1,'removePCB(PCB *pcb):&#160;pcb.c'],['../pcb_8c.html#a3a96f518e7e034c3bc917ba2f0efbcd2',1,'removePCB(PCB *pcb):&#160;pcb.c']]],
  ['repr_5fclas',['repr_clas',['../pcb_8h.html#accf6a8e08f7a8236a8802ea9b1486d33',1,'repr_clas(int clas):&#160;pcb.c'],['../pcb_8c.html#accf6a8e08f7a8236a8802ea9b1486d33',1,'repr_clas(int clas):&#160;pcb.c']]],
  ['repr_5fstate',['repr_state',['../pcb_8h.html#ac077b05cbcefd172adbd5d43877a024e',1,'repr_state(int rrb):&#160;pcb.c'],['../pcb_8c.html#ac077b05cbcefd172adbd5d43877a024e',1,'repr_state(int rrb):&#160;pcb.c']]],
  ['repr_5fstatus',['repr_status',['../pcb_8h.html#abd2b790fdde69c100bb8f330e9f64b39',1,'repr_status(int sus):&#160;pcb.c'],['../pcb_8c.html#abd2b790fdde69c100bb8f330e9f64b39',1,'repr_status(int sus):&#160;pcb.c']]],
  ['reserved',['reserved',['../interrupts_8c.html#ad686e3fee8ec8346a6d8e98d970a02dd',1,'interrupts.c']]],
  ['rtc_5fisr',['rtc_isr',['../interrupts_8c.html#a52f2615cebbdeab188085a03c913fcf9',1,'interrupts.c']]]
];
