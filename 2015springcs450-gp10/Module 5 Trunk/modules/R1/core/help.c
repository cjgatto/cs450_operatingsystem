#include <modules/R1/commhand.h>
#include <modules/R1/help.h>
#include <core/serial.h>
#include <string.h>

/**
  Processes and handles the help command.
  @return int - signals the continuation of the loop.
*/
  
int handle_help(void) {
  char *command = strtok(NULL, " ");

  int i;
  for(i = 0; i < COMM_COUNT; i++) {
  	if(strcmp(command, commands[i].text) == 0){
  		serial_println(commands[i].help);
  		return CONTINUE;
  	}
  }
  if(command == NULL) {
    serial_println(commands[2].help);
    serial_println("Commands:\n\tshutdown\n\tversion\n\thelp\n\tgetdate\n\tsetdate\n\tgettime\n\tsettime\n\tsuspend\n\tresume\n\tsetpriority\n\tshowpcb\n\tshowall\n\tshowready\n\tshowblocked\n\tcreatepcb\n\tdeletepcb\n\tblock\n\tunblock\n\tclearqueues\n\tyield\n\tloadr3\n\tresumeall\n\tshowallocated\n\tshowfree\n\tisempty\n\tshowheap\n\tinitheap\n\tfreemcb\n\tallocatedmcb\n");
    return CONTINUE;
  }
  serial_print("(Help): ");
  serial_print(command);
  serial_println(": Help text not found.");

  return CONTINUE;
}
