#include <modules/R2/pcb.h>
#include <core/serial.h>
#include <system.h>
#include <mem/heap.h>
#include <modules/mpx_supt.h>
#include <string.h>
#include <core/stack.h>
#include <modules/R3/context.h>
#include <core/dispatcher.h>

QUEUE readyQueue;
QUEUE blockedQueue;

/*******************************************************
 * COMMAND HANDLERS
 *******************************************************/

//**********************
// Permanent Handlers
//**********************

/**
  Changes the state of a PCB to be suspended.
  @return int- Error code if applicable
*/
int handle_suspend() {
	char* name = strtok(NULL, " ");

	if(name == NULL) {
		serial_println("Error: Invalid Parameters");
		return ERROR;
	}

	PCB* pcb = findPCB(name);

	if(pcb == NULL) {
		serial_println("Error: A PCB of that name does not exist.");
		return ERROR;
	}
	
	pcb->sus = SUSPENDED;

	return CONTINUE;
}

/**
  Changes the state of a PCB to not be suspended.
  @return int- Error code if applicable: Checks name
*/
int handle_resume() {
	char* name = strtok(NULL, " ");

	if(name == NULL) {
		serial_println("Error: Invalid Parameters");
		return ERROR;
	}

	PCB* pcb = findPCB(name);

	if(pcb == NULL) {
		serial_println("Error: A PCB of that name does not exist.");
		return ERROR;
	}

	pcb->sus = NOT_SUSPENDED;
	
	return CONTINUE;
}

/**
  Changes the priority of a PCB based on inital input.
  @return int- Error code if applicable: Checks name, priority range
*/
int handle_setpriority() {
	char* name = strtok(NULL, " ");
	char* prio_char = strtok(NULL, " ");

	if(name == NULL || prio_char == NULL) {
		serial_println("Error: Invalid Parameters");
		return ERROR;
	}

	int prio = atoi(prio_char);

	if(prio < 0 || prio > 9) {
		serial_println("Error: The priority is invalid.");
		return ERROR;
	}

	PCB* pcb = findPCB(name);

	if(pcb == NULL) {
		serial_println("Error: A PCB of that name does not exist.");
		return ERROR;
	}
	if(pcb->priority==prio)return CONTINUE;

	pcb->priority = prio;

	if(pcb->rrb != BLOCKED) {
		removePCB(pcb);
		insertPCB(pcb);
	}

	return CONTINUE;
}

/**
  Displays the name, class, states, and priority of a PCB.
  @return int- Error code if applicable: Checks name
*/
int handle_showpcb() {
	char* name = strtok(NULL, " ");
	PCB* pcb = findPCB(name);

	if(pcb != NULL) {
		displayPCB(pcb);
	}
	else {
		serial_println("Error: A PCB of that name does not exist.");
	}

	strclr(name);//why?
	return CONTINUE;
}

/**
  Displays information for each PCB in the ready queue.
  @return int- Error code if applicable
*/
int handle_showready() {
	PCB* pcb = readyQueue.head;

	while(pcb != NULL) {
		displayPCB(pcb);
		pcb = pcb->next;
	}

	return CONTINUE;
}

/**
  Displays information for each PCB in the blocked queue.
  @return int- Error code if applicable
*/
int handle_showblocked() {
	PCB* pcb = blockedQueue.head;

	while(pcb != NULL) {
		displayPCB(pcb);
		pcb = pcb->next;
	}

	return CONTINUE;
}

/**
  Displays information for each PCB in both queues.
  @return int- Error code if applicable
*/
int handle_showall() {
	serial_println("Ready Queue:");
	handle_showready();
	serial_println("Blocked Queue:");
	handle_showblocked();
	return CONTINUE;
}


//**********************
// Temporary Handlers
//**********************

/**
  Constructs a PCB based on initial user input.
  @return int- Error code if applicable: Checks name, class, states, and priority
*/
int handle_createpcb() {
	char* name = strtok(NULL, " ");
	char* clas_cptr = strtok(NULL, " ");
	char* prio_cptr = strtok(NULL, " ");

	if(name == NULL || clas_cptr == NULL || prio_cptr == NULL) {
		serial_println("Error: Invalid Parameters.");
		return ERROR;
	}

	if(strlen(name) > 20) {
		serial_println("Error: Name is too long (over 20 characters).");
		return ERROR;
	}

	int clas = atoi(clas_cptr);
	int prio = atoi(prio_cptr);

	if(findPCB(name) != NULL) {
		serial_println("Error: A PCB of that name already exists.");
		return ERROR;
	}

	if(clas < 1 || clas > 2) {
		serial_println("Error: An invalid PCB type has been supplied.");
		return ERROR;
	}

	if(prio < 0 || prio > 9) {
		serial_println("Error: The priority is invalid.");
		return ERROR;
	}
	
	insertPCB(setupPCB(name, clas, prio));

	return CONTINUE;
}

/**
  Finds and initiates the removal of a PCB.
  @return int- Error code if applicable: Checks name
*/
int handle_deletepcb() {
	char* name = strtok(NULL, " ");

	if(name == NULL) {
		serial_println("Error: Invalid Parameters");
		return ERROR;
	}

	PCB* pcb = findPCB(name);

	if(pcb != NULL) {
		removePCB(pcb);
		freePCB(pcb);
	}
	else {
		serial_println("Error: A PCB of that name does not exist.");
		return ERROR;
	}

	return CONTINUE;
}

/**
  Changes the state of a PCB to blocked and places it into the appropriate queue.
  @return int- Error code if applicable: Checks name
*/
int handle_block() {
	char* name = strtok(NULL, " ");

	if(name == NULL) {
		serial_println("Error: Invalid Parameters");
		return ERROR;
	}

	PCB* pcb = findPCB(name);

	if(pcb == NULL) {
		serial_println("Error: A PCB of that name does not exist.");
		return ERROR;
	}
	removePCB(pcb);
	pcb->rrb = BLOCKED;
	insertPCB(pcb);

	return CONTINUE;
}

/**
  Changes the state of a PCB to unblocked and places it into the appropriate queue.
  @return int- Error code if applicable: Checks name
*/
int handle_unblock() {
	char* name = strtok(NULL, " ");

	if(name == NULL) {
		serial_println("Error: Invalid Parameters");
		return ERROR;
	}

	PCB* pcb = findPCB(name);

	if(pcb == NULL) {
		serial_println("Error: A PCB of that name does not exist.");
		return ERROR;
	}
	if(pcb->rrb!=BLOCKED){
		serial_println("Error: This PCB is not blocked.");
		return ERROR;
	}
	removePCB(pcb);
	pcb->rrb = READY;
	insertPCB(pcb);

	return CONTINUE;
}

int handle_resumeall() {
  PCB* curr = readyQueue.head;

  while(curr) {
		curr->sus = NOT_SUSPENDED;
		curr = curr->next;
  }

	curr = blockedQueue.head;

  while(curr) {
		curr->sus = NOT_SUSPENDED;
		curr = curr->next;
  }

	return CONTINUE;
}

/**
  Clears both queues of all PCBs. (Might not be temporary)
  @return int- Error or Success code
*/
int handle_clearqueues(){  
  PCB* pcb = readyQueue.head;
  while(pcb) {
    removePCB(pcb);
    freePCB(pcb);
    pcb = readyQueue.head;
  }

  pcb = blockedQueue.head;
  while(pcb) {
    removePCB(pcb);
    freePCB(pcb);
    pcb= blockedQueue.head;
  }

  serial_println("Queues Cleared.");
  insertIdle();
  return CONTINUE;
}

 /*******************************************************
 * PROCEDURES
 *******************************************************/

/**
  Uses sys_alloc_mem(...) to allocate memory for a new PCB.
  @return PCB*- Pointer to the PCB, defaults to NULL if faulty
*/
PCB* allocatePCB() {
	PCB* pcb = NULL;
	pcb = (PCB *) sys_alloc_mem(sizeof(PCB));
	pcb->stackTop = pcb->stackBase+1024-sizeof(context);
	return pcb; 
}

/**
  Uses sys_free_mem(...) to free the memory associated with a PCB.
  @param PCB* pcb- PCB to free
  @return int- Error or Success code
*/
int freePCB(PCB *pcb) {
	if(sys_free_mem(pcb) == -1) {
		return CONTINUE;
	}
	return ERROR;
}

/**
  Uses allocatePCB() and initializes the data of the PCB.
  @param char* name- Name of the PCB
  @param int clas- Class type of the PCB
  @param int priority- Priority of the PCB
  @return PCB*- A pointer to the created PCB.
*/
PCB* setupPCB(char *name, int clas, int priority) {
	if(findPCB(name)) {
		return NULL;
	}

	struct PCB* pcb = allocatePCB();

	if(pcb == NULL) {
		return NULL;
	}

	strcpy(pcb->name, name);
	pcb->name[20] = '\0';
	pcb->clas = clas;
	pcb->rrb = READY;
	pcb->sus = NOT_SUSPENDED;
	pcb->priority = priority;
	return pcb;
}

/**
  Searches both queues for the specified PCB.
  @param char* name- Name of a PCB
  @return PCB*- Pointer to the located PCB, NULL if not found
*/
PCB* findPCB(char* name) {
	PCB* curr=readyQueue.head; // search ready queue first
	while(curr){
		if(strcmp(name,curr->name)==0) {
			return curr;
		}
		curr=curr->next;
	}

	curr=blockedQueue.head; // search blocked queue second
	while(curr){
		if(strcmp(name,curr->name)==0) {
			return curr;
		}
		curr=curr->next;
	}
	return NULL;
}

/**
  Identifies what queue the PCB belongs in and inserts it accordingly.
  @param PCB* pcb- PCB to insert into a queue
*/
void insertPCB(PCB* pcb) {
  if(!pcb) {
    return;
  }

	if(pcb->rrb == READY) { // place into readyQueue
		if(readyQueue.count == 0) { // If the queue is empty
			readyQueue.head = readyQueue.tail = pcb;
			pcb->next = NULL;
			pcb->prev = NULL;
			readyQueue.count++;
			return;
		}
		else { // The queue is not empty...
			PCB* curr = readyQueue.head;

			if(pcb->priority > curr->priority) { // If its priority is greater than head
				readyQueue.head = pcb;
				pcb->next = curr;
				curr->prev = pcb;
				readyQueue.count++;
				return;
			}

			curr = curr->next;

			while(curr) { // Place it given its priority within the queue
				if(pcb->priority > curr->priority) {
					curr->prev->next = pcb;
					pcb->prev = curr->prev;
					pcb->next = curr;
					curr->prev = pcb;
					readyQueue.count++;
					return;
				}
				curr = curr->next;
			}
			// If pcb is the lowest priority, place it at the tail
			curr = readyQueue.tail;
			curr->next = pcb;
			pcb->prev = curr;
			pcb->next = NULL;
			readyQueue.tail = pcb;
			readyQueue.count++;
			return;
		}
	}
	else if(pcb->rrb == BLOCKED) { //place into blockedQueue
		if(blockedQueue.count == 0) { // If the queue is empty
			blockedQueue.head = blockedQueue.tail = pcb;
			pcb->next = NULL;
			pcb->prev = NULL;
			blockedQueue.count++;
			// serial_println("Blocked: added initial"); // DEBUG
			return;
		}
		else { // The queue is not empty...insert at tail (FIFO)
			blockedQueue.tail->next = pcb;
			pcb->prev = blockedQueue.tail;
			pcb->next = NULL;
			blockedQueue.tail = pcb;
			blockedQueue.count++;
			return;
		}
	}
	else { //The state is unkown
		return;
	}
}

/**
  Locates and removes the specified PCB.
  @param PCB* pcb- PCB to be removed
  @return int- Success or Error code
*/
int removePCB(PCB* pcb){
	if(pcb->rrb == READY) { // remove from readyQueue
		if(readyQueue.head == pcb && readyQueue.tail == pcb) {
			readyQueue.head = readyQueue.tail = NULL;
		}
		else if(readyQueue.head == pcb) {
			readyQueue.head = pcb->next;
			readyQueue.head->prev = NULL;
		}
		else if(readyQueue.tail == pcb) {
			readyQueue.tail = pcb->prev;
			readyQueue.tail->next = NULL;
		}
		else {
			pcb->next->prev = pcb->prev;
			pcb->prev->next = pcb->next;
		}
		readyQueue.count--;
	}
	else if(pcb->rrb == BLOCKED) { // remove from blocked
		if(blockedQueue.head == pcb && blockedQueue.tail == pcb) {
			blockedQueue.head = blockedQueue.tail = NULL;
		}
		else if(blockedQueue.head == pcb) {
			blockedQueue.head = pcb->next;
			blockedQueue.head->prev = NULL;
		}
		else if(blockedQueue.tail == pcb) {
			blockedQueue.tail = pcb->prev;
			blockedQueue.tail->next = NULL;
		}
		else {
			pcb->next->prev = pcb->prev;
			pcb->prev->next = pcb->next;
		}
		blockedQueue.count--;
	}
	else { // The state is unkown
		return ERROR;
	}
	pcb->next=NULL;
	pcb->prev=NULL;
	//insertIdle();
	return CONTINUE;
}

/*******************************************************
 * HELPER FUNCTIONS
 *******************************************************/

/**
  Displays meaningful information of the supplied PCB.
  @param PCB* pcb- PCB to be displayed
*/
void displayPCB(PCB* pcb) {
	char temp[2];
	temp[1] = '\0';
	serial_print("\tPCB    :  ");
	serial_println(pcb->name);
	serial_print("\tClass  :  ");
	serial_println(repr_clas(pcb->clas));
	serial_print("\tState  :  ");
	serial_println(repr_state(pcb->rrb));
	serial_print("\tStatus :  ");
	serial_println(repr_status(pcb->sus));
	temp[0] = (char) pcb->priority + 48;
	serial_print("\tPrio   :  ");
	serial_println(temp);
	serial_println("");
}

/**
  Provides the string representation of the suspention state.
  @param int- Suspension status to convert
  @return char*- Text representation of the suspension status
*/
char* repr_status(int sus) {
	if(sus == SUSPENDED) {
		return "Suspended";
	}
	else if(sus == NOT_SUSPENDED) {
		return "Not Suspended";
	}
	return "ERROR";
}

/**
  Provides the string representation of the ready, running, blocked state.
  @param int- Ready, running, blocked state to convert
  @return char*- Text representation of the state
*/
char* repr_state(int rrb) {
	if(rrb == READY) {
		return "Ready";
	}
	else if(rrb == RUNNING) {
		return "Running";
	}
	else if(rrb == BLOCKED) {
		return "Blocked";
	}
	return "ERROR";
}

/**
  Provides the string representation of the class.
  @param int- System or Application to convert
  @return char*- Text representation of the class
*/
char* repr_clas(int clas) {
	if(clas == SYSTEM) {
		return "System";
	}
	else if(clas == APPLICATION) {
		return "Application";
	}
	return "ERROR";
}

/**
  Gets the head of the readyqueue
  @param none
  @return PCB*- The head of the readyqueue
*/
PCB* get_ready_proc() {
	PCB* curr = readyQueue.head;

	while(curr) {
		if(curr->sus == NOT_SUSPENDED) {
			return curr;
		}
		curr = curr->next;
	}
	return NULL;
}
