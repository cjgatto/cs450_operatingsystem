#include <modules/R5/memory_management.h>

List free_blocks;
List allocated_blocks;
ucstar heap_start;
ucstar heap_end;

/**
  Initializes the memory manager's heap space for process allocations.
  @param int size - Size of free space for the heap
  @return int - Success code describing completion
*/
int init_heap(int size) {
	int adjusted_size = size + sizeof(MCB);
	while(adjusted_size%(sizeof(unsigned char *) != 0)) {
		adjusted_size++;
	}

	heap_start = kmalloc(adjusted_size);
	if(!heap_start) {
		return -1;
	}

	MCB* head = (MCB*) heap_start;
	fill_mcb(head, FREE, adjusted_size, get_usable_address(head), "INITIAL_FREE_BLOCK", NULL, NULL);
	heap_end = mcb_shift(heap_start, head->size);

	insert_mcb(head);
	allocated_blocks.head = NULL;

	return adjusted_size;
}

/**
  Initializes the memory manager's heap space for process allocations.
  @param u32int siz - Size of free space for the allocation
  @return u32int - First useable address
*/
u32int my_alloc(u32int siz) {
	if(siz < 1) {
		return 0;
	}

	int size_required = (int) siz + (int)sizeof(MCB);
	int curr_size;

	MCB* curr = free_blocks.head;
	while(curr) {
		if(curr->size >= size_required) {
			curr_size = curr->size;
			break;
		}
		curr = curr->next;
	}

	if(!curr) {
		serial_println("Error: Allocation too large.");
		return ERROR;
	}

	remove_mcb(curr);

	MCB* amcb = curr;

	if((curr->size - size_required) < (int) sizeof(MCB)) {
		size_required = curr->size;
		fill_mcb(amcb, ALLOCATED, size_required, get_usable_address(amcb), NULL, NULL, NULL);
		insert_mcb(amcb);
	}
	else {
		fill_mcb(amcb, ALLOCATED, size_required, get_usable_address(amcb), NULL, NULL, NULL);
		insert_mcb(amcb);

		MCB* fmcb = amcb;
		fmcb = (MCB*) mcb_shift((ucstar) fmcb, size_required);
		int fmcb_size = curr_size - amcb->size;
		fill_mcb(fmcb, FREE, fmcb_size, get_usable_address(fmcb), "EXCESS_FREE_BLOCK\0", NULL, NULL);

		insert_mcb(fmcb);
	}
	return (u32int) amcb->addr;
}

/**
  Frees a MCB from the heap, creating a new FMCB in its place. The new FMCB will attempt
  to join with its neighboring FMCBs.
  @param void *p - Pointer to the memory being freed.
  @return int - Success code describing completion
*/
int my_free(void *p) {
	MCB* mcb = findParent((ucstar) p);

	if(!mcb) {
		return ERROR;
	}
	int new_size = mcb->size;
	ucstar usable_addr = get_usable_address(mcb);

	remove_mcb(mcb);
	fill_mcb(mcb, FREE, new_size, usable_addr, "FREE_BLOCK", NULL, NULL);
	insert_mcb(mcb);

	MCB* prev = heap_previous_mcb(mcb);
	MCB* next = heap_next_mcb(mcb);
	MCB* new_mcb = mcb;

	if(prev && prev != mcb && prev->type == FREE) {
		remove_mcb(prev);
		new_mcb = prev;
		new_size += prev->size;
	}

	if(next && next != mcb && next->type == FREE) {
		remove_mcb(next);
		new_size += next->size;
	}

	if(new_size != mcb->size) {
		remove_mcb(mcb);
		fill_mcb(new_mcb, FREE, new_size, get_usable_address(new_mcb), "FREE_BLOCK", NULL, NULL);
		insert_mcb(new_mcb);
	}
	return 0;
}

/**
  Searches the heap for the MCB directly before the current MCB
  @param MCB* mcb - Free MCB to search from
  @return MCB* - Pointer to the previous MCB in the heap
*/
MCB* heap_previous_mcb(MCB* mcb) {
	MCB* prev_fmcb = mcb->prev;
	MCB* prev_amcb = heap_previous_allocated(mcb);

	if(!prev_fmcb) {
		return prev_amcb;
	}
	if(!prev_amcb) {
		return prev_fmcb;
	}
	return prev_fmcb > prev_amcb ? prev_fmcb : prev_amcb;
}

/**
  Searches the heap for the Allocate MCB directly before the current MCB
  @param MCB* mcb - Free MCB to search from
  @return MCB* - Pointer to the previous MCB in the heap
*/
MCB* heap_previous_allocated(MCB* mcb) {
	MCB* prev = NULL;
	MCB* curr = allocated_blocks.head;

	while(curr) {
		if(curr > mcb) {
			break;
		}
		prev = curr;
		curr = curr->next;
	}
	return prev;
}

/**
  Searches the heap for the MCB directly after the current MCB
  @param MCB* mcb - Free MCB to search from
  @return MCB* - Pointer to the next MCB in the heap
*/
MCB* heap_next_mcb(MCB* mcb) {
	MCB* next_fmcb = mcb->next;
	MCB* next_amcb = heap_next_allocated(mcb);

	if(!next_fmcb) {
		return next_amcb;
	}
	if(!next_amcb) {
		return next_fmcb;
	}
	return next_fmcb < next_amcb ? next_fmcb : next_amcb;
}

/**
  Searches the heap for the Allocated MCB directly after the current MCB
  @param MCB* mcb - Free MCB to search from
  @return MCB* - Pointer to the next MCB in the heap
*/
MCB* heap_next_allocated(MCB* mcb) {
	MCB* curr = allocated_blocks.head;

	while(curr) {
		if(curr < mcb) {
			if(curr->next && curr->next > mcb)  {
				return curr;
			}
		}
		curr = curr->next;
	}
	return NULL;
}

/**
  Mini-constructor for populating MCB fields
  @param MCB* mcb - MCB to populate
  @param int type - Free or Allocated
  @param int size - Usable space
  @param ucstar addr - Location of the first usable address
  @param char* name - Name of the PCB
  @param MCB* next - Pointer to the next MCB in its respective list
  @param MCB* prev - Pointer to the previous MCB in its respective list
*/
void fill_mcb(MCB* mcb, int type, int size, ucstar addr, char* name, MCB* next, MCB* prev) {
	mcb->type = type;
	mcb->size = size;
	mcb->addr = addr;
	mcb->PCBName = name;
	mcb->next = next;
	mcb->prev = prev;
}

/**
  Removes an MCB from its respective list
  @param MCB* mcb - MCB to remove
*/
void remove_mcb(MCB* mcb) {
	if(mcb->type == FREE) {
		remove_free_mcb(mcb);
	}
	else if(mcb->type == ALLOCATED) {
		remove_allocated_mcb(mcb);
	}
	else {
		return;
	}
}

/**
  Removes an MCB from the Free list
  @param MCB* mcb - MCB to remove
*/
void remove_free_mcb(MCB* mcb) {
	if(compare_mcb(mcb, free_blocks.head) == 0) {
		if(free_blocks.head->next) {
			free_blocks.head = free_blocks.head->next;
			free_blocks.head->prev = NULL;
		}
		else {
			free_blocks.head = NULL;
		}

	}
	else if(mcb->next == NULL) {
		mcb->prev->next = NULL;
	}
	else {
		mcb->prev->next = mcb->next;
		mcb->next->prev = mcb->prev;
	}
}

/**
  Searches the heap for the Allocated MCB directly after the current MCB
  @param MCB* mcb - MCB to remove
*/
void remove_allocated_mcb(MCB* mcb) {
	if(compare_mcb(mcb, allocated_blocks.head) == 0) {
		if(allocated_blocks.head->next) {
			allocated_blocks.head = allocated_blocks.head->next;
			allocated_blocks.head->prev = NULL;
		}
		else {
			allocated_blocks.head = NULL;
		}

	}
	else if(mcb->next == NULL) {
		mcb->prev->next = NULL;
	}
	else {
		mcb->prev->next = mcb->next;
		mcb->next->prev = mcb->prev;
	}
}

/**
  Inserts a MCB to its respective list, ordered by increasing address.
  @param MCB* mcb - MCB to insert
*/
void insert_mcb(MCB* mcb) {
	if(mcb->type == FREE) {
		insert_free_mcb(mcb);
	}
	else if(mcb->type == ALLOCATED) {
		insert_allocated_mcb(mcb);
	}
	else {
		return;
	}
}

/**
  Inserts a MCB to the Free list, ordered by increasing address.
  @param MCB* mcb - MCB to insert
*/
void insert_free_mcb(MCB* mcb) {
	MCB* curr;
	if(free_blocks.head == NULL) {
		free_blocks.head = mcb;
	}
	else if(mcb < free_blocks.head) {
		free_blocks.head->prev = mcb;
		mcb->next = free_blocks.head;
		free_blocks.head = mcb;
	}
	else {
		curr = free_blocks.head;
		while(curr->next != NULL) {
			if(mcb < curr) {
				mcb->prev = curr->prev;
				mcb->prev->next = mcb;
				mcb->next = curr;
				curr->prev = mcb;
				return;
			}
			curr = curr->next;
		}
		if(mcb < curr) {
			mcb->prev = curr->prev;
			mcb->prev->next = mcb;
			mcb->next = curr;
			curr->prev = mcb;
		}
		else {
			curr->next = mcb;
			mcb->prev = curr;
		}
	}
}

/**
  Inserts a MCB to the Allocated list, ordered by increasing address.
  @param MCB* mcb - MCB to insert
*/
void insert_allocated_mcb(MCB* mcb) {
	MCB* curr;
	if(allocated_blocks.head == NULL) {
		allocated_blocks.head = mcb;
	}
	else if(mcb < allocated_blocks.head) {
		allocated_blocks.head->prev = mcb;
		mcb->next = allocated_blocks.head;
		allocated_blocks.head = mcb;
	}
	else {
		curr = allocated_blocks.head;
		while(curr->next != NULL) {
			if(mcb < curr) {
				mcb->prev = curr->prev;
				mcb->prev->next = mcb;
				mcb->next = curr;
				curr->prev = mcb;
				return;
			}
			curr = curr->next;
		}
		if(mcb < curr) {
			mcb->prev = curr->prev;
			mcb->prev->next = mcb;
			mcb->next = curr;
			curr->prev = mcb;
		}
		else {
			curr->next = mcb;
			mcb->prev = curr;
		}
	}
}

/**
  Determines if two MCB point to the same location.
  @param MCB* m1 - MCB to compare
  @param MCB* m2 - MCB to compare
  @return int - Results of comparison
*/
int compare_mcb(MCB* m1, MCB* m2) {
	int comp = m1->addr - m2->addr;
	return comp;
}

/**
  Handles the user command to add an MCB to the system
  @return int - Results of the allocation
*/ // [REMOVE]
int handle_allocate_mcb(void) {
	char* size_str = strtok(NULL, " ");

	if(size_str == NULL) {
		serial_println("Allocate MCB: Too few arguments");
		return CONTINUE;
	}

	int size = atoi(size_str) + (int) heap_start;
	u32int result = my_alloc(size);

	if((int)result == ERROR_REQUEST_TOO_SMALL) {
		serial_println("Allocation Failed: The requested size was too small");
	}
	else if((int)result == ERROR_SPACE_UNAVAILABLE) {
		serial_println("Allocation Failed: Insufficient free space");
	}
	else {
		serial_println("The allocation completed successfully.");
	}
	return CONTINUE;
}

/**
  Handles the user command to manually initialize a heap
  @return int - Results of the heap initialization
*/ // [REMOVE]
int handle_init_heap(void) {
	int HEAP_SIZE = 40000;
	
	if(init_heap(HEAP_SIZE)) {
		serial_println("The heap has been initialized successfully.");
		serial_print("\tSize:\t");
		char temp[6];
		temp[5] = '\0';
		itoa(HEAP_SIZE, temp);
		serial_println(temp);
	}
	else {
		serial_println("Heap allocation failed.");
	}
	return CONTINUE;
}

/**
  Handles the user command to remove a MCB from the heap
  @return int - Results of the removal
*/ // [REMOVE]
int handle_free_mcb(void) {
	void *p = (void *) strtok(NULL, " ");

	if(p == NULL) {
		serial_println("Free MCB: Too few arguments");
		return CONTINUE;
	}

	int result = my_free(p);

	if(result == 0) {
		serial_println("Free Failed: The specified MCB is incapable of being freed.");
	}
	else {
		serial_println("The MCB has been freed successfully.");
	}
	return CONTINUE;
}

/**
  Handles the user command to print the free and allocated lists
  @return int - Results of the printing
*/ // [REMOVE]
int handle_print_lists(void) {
	handle_print_alloc();
	handle_print_free();
	return CONTINUE;
}

/**
  Handles the user command to print the layout of the heap
  @return int - Results of the heap printing
*/
int handle_print_heap(void) {
	MCB* curr_a = allocated_blocks.head;
	MCB* curr_f = free_blocks.head;

	while(curr_a && curr_f) {
		if(curr_a < curr_f) {
			serial_println("\n\tALLOCATED");
			print_block(curr_a);
			curr_a = curr_a->next;
		}
		else {
			serial_println("\n\tFREE");
			print_block(curr_f);
			curr_f = curr_f->next;
		}
	}
	while(curr_a) {
		serial_println("\n\tALLOCATED");
		print_block(curr_a);
		curr_a = curr_a->next;
	}
	while(curr_f) {
		serial_println("\n\tFREE");
		print_block(curr_f);
		curr_f = curr_f->next;
	}
	return CONTINUE;
}

/**
  Handles the user command to print the allocated list
  @return int - Results of the printing
*/
int handle_print_alloc(void) {
	serial_println("\nAllocated:\n");
	MCB* curr = allocated_blocks.head;
	while(curr) {
		print_block(curr);
		curr = curr->next;
	}
	return CONTINUE;
}

/**
  Handles the user command to print the free list
  @return int - Results of the printing
*/
int handle_print_free(void) {
	serial_println("\nFree:\n");
	MCB* curr = free_blocks.head;
	MCB* prev = NULL;
	while(curr && curr->type == FREE && prev != curr) {
		print_block(curr);
		prev = curr;
		curr = curr->next;
	}
	return CONTINUE;
}

/**
  Prints the datum of a single MCB
  @param MCB* curr - MCB to print
*/
void print_block(MCB* curr) {
	serial_print("\tSize: ");
	char size_str[6];
	size_str[5] = '\0';
	itoa(curr->size, size_str);
	serial_println(size_str);
}

/**
  Handles the user command to determine if the heap is empty
  @return int - Retunrs continue on success
*/
int handle_print_isEmpty(void) {
	if(is_empty()) {
		serial_println("The heap is empty.");
	}
	else {
		serial_println("The heap is not empty.");
	}
	return CONTINUE;
}

/**
  Determines if the heap is empty
  @return int - Results if allocated list is empty
*/
int is_empty(void) {
	return allocated_blocks.head == NULL;
}

/**
  Shifts the MCB pointer a specified amount
  @param ucstar mcb_ptr - MCB pointer to shift
  @param int shift - Amount to shift the MCB pointer by
  @return ucstar - Shifted MCB pointer
*/
ucstar mcb_shift(ucstar mcb_ptr, int shift) {
	ucstar ptr = mcb_ptr;
	ptr += shift;
	return ptr;
}

/**
  Finds the first usable location within the MCB by shifting the MCB pointer
  by the size of an MCB
  @param MCB* mcb - MCB to find the first usable location of
  @return uctar - Pointer to the first useable address
*/
ucstar get_usable_address(MCB* mcb) {
	ucstar addr = (ucstar) mcb;
	int i;
	for(i=0; i<(int)sizeof(MCB); i++) {
		addr++;
	}
	return (ucstar) addr;
}

/**
  Finds a given MCB pointer in the list of allocated MCBs
  @param ucstar addr - Address to search for within the allocated list
  @return MCB* - Pointer to the located MCB, otherwise NULL
*/
MCB* findMCB(ucstar addr) {
	MCB* curr = allocated_blocks.head;
	while(curr) {
		if(compare_mcb(curr, (MCB*) addr) == 0) {
			return curr;
		}
		curr = curr->next;
	}
	return NULL;
}

/**
  Given the pointer of an MCB's "first usable location," find the MCB contiaing
  this address
  @param ucstar p - Pointer to the first useable address
  @return MCB* - Pointer to the MCB containing address p, otherwise NULL
*/
MCB* findParent(ucstar p) {
	ucstar parent_addr = mcb_shift(p, -1*sizeof(MCB));
	return findMCB(parent_addr);
}
