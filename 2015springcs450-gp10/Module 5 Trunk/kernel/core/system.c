#include <string.h>
#include <system.h>
#include <modules/mpx_supt.h>
#include <core/io.h>
#include <core/serial.h>
#include <modules/R2/pcb.h>
#include <modules/R3/context.h>

PCB *cop;
context *global_context; // Global context set by sys_call

/*
  Procedure..: klogv
  Description..: Kernel log messages. Sent to active
      serial device.
*/
void klogv(const char *msg)
{
  char logmsg[64] = {'\0'}, prefix[] = "klogv: ";
  strcat(logmsg, prefix);
  strcat(logmsg, msg);
  serial_println(logmsg);
}

/*
  Procedure..: kpanic
  Description..: Kernel panic. Prints an error message
      and halts.
*/
void kpanic(const char *msg)
{
  cli(); //disable interrupts
  char logmsg[64] = {'\0'}, prefix[] = "Panic: ";
  strcat(logmsg, prefix);
  strcat(logmsg, msg);
  klogv(logmsg);
  hlt(); //halt
}

/*
  Procedure..: sys_call
  Description..: Handles interrupts in commhand
 */
u32int* sys_call(context *registers) {
	PCB *pcb = get_ready_proc();
	if (cop == NULL) {
		global_context = registers;
	} 	
	else {
		if(get_op_code() == IDLE) {
			cop->stackTop = (unsigned char*)registers;
			//serial_print("Inserting: ");
			//serial_println(pcb->name);
			insertPCB(cop);
		} 
		else if (get_op_code() == EXIT) {
			freePCB(cop);
		}
	}

	if (pcb != NULL) {
		cop = pcb;
//PCB *pcb = get_ready_proc();
//serial_print("Removing: ");
//serial_println(pcb->name);
		
		removePCB(pcb);
		outb(0x20, 0x20);
		return (u32int*) cop->stackTop;
	}
	cop = NULL;
	return (u32int*) global_context;
}
