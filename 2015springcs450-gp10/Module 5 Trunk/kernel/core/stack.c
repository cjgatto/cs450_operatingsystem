#include <system.h>
#include <core/stack.h>
#include <core/serial.h>

struct Stack {
	char **data;
	int size;
};
typedef struct Stack Stack;

void init_stack(Stack *stack) {
	stack->size = 0;
}

char* top_stack(Stack *stack) {
	if((*stack).size != 0) {
		return stack->data[stack->size - 1];
	}
	return NULL;
}

void push_stack(Stack *stack, char *command) {
	serial_print("<In stack, pushing: ");
	serial_print(command);
	serial_print(">");
	if(stack->size < STACK_SIZE) {
		stack->data[stack->size] = command;
	}
}

void pop_stack(Stack *stack) {
	if(stack->size != 0) {
		stack->size--;
	}
}