#include <system.h>
#include <modules/R3/context.h>
#include <core/dispatcher.h>
#include <string.h>
#include "modules/mpx_supt.h"

/**
  Creates PCB, its associated context, and sets its status to SUSPENDED.
  @param char* name- Name of the PCB
  @param function pointer- function to bind to the context
  @return PCB*- A pointer to the created PCB.
*/
PCB* loader(char* name, void (*func)(void)) {

  PCB *new_pcb = setupPCB(name, 1, 1);

  if(findPCB(name)) {
    return NULL;
  }

  new_pcb->sus = SUSPENDED;
  context *cp = (context*)(new_pcb->stackTop);
  memset(cp, 0, sizeof(context));
  cp->fs = 0x10;
  cp->gs = 0x10;
  cp->ds = 0x10;
  cp->es = 0x10;
  cp->cs = 0x8;
  cp->ebp = (u32int)(new_pcb->stackBase);
  cp->esp = (u32int)(new_pcb->stackTop);
  cp->eip = (u32int)func;
  cp->eflags = 0x202;
  return new_pcb;
}

/**
  Calls the interrupt on line 60, triggering a context switch
*/
void yield(void) {
  asm volatile ("int $60");
}

void insertIdle() {
  PCB* idl = loader("idle", &idle);
  idl -> priority = 0;
  idl -> sus = NOT_SUSPENDED;
  insertPCB(idl);
}