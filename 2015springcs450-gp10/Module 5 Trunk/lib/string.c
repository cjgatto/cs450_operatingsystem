/**
  String contains functions use to operate on and manipulate character arrays.
*/

#include <system.h>
#include <string.h>
#include <mem/heap.h>

#include <core/serial.h>

/**
  Returns the length of a string.
  @param const char* s-string to be measured
  @return int-length of the string
*/
int strlen(const char *s)
{
  int len;

  for(len=0; *s!='\0'; s++) {
    len++;
  }

  return len;
}

/**
  Turns all capital letters(A-Z) in string to lower case.
  @param char* s1-string to be changed
*/
void toLower(char*s1){
 	while(*s1!='\0'){
 		if(*s1>='A'&&*s1<='Z'){
 			*s1=(*s1)+32;
 		}
 		s1++;
 	}	
 }

/**
  Turns all lower case letters(a-z) in string to capitals.
  @param char* s1-string to be changed 
*/
void toUpper(char*s1){
	while(*s1!='\0'){
		if(*s1>='a'&&*s1<='z'){
 			*s1=(*s1)-32;
 		}
 		s1++;
 	}
 }

/**
  Copy one string to another location.
  @param char* s1-destination of copy
  @param const char* s2-string to be copied
  @return char*- new string
*/
char* strcpy(char *s1, const char *s2)
{
  char *start;
  start = s1;

  while((*s1++ = *s2++) != '\0');
  
  return start;
}

/**
  Converts an ASCII string to an integer
  @param const char* s-string to be converted
  @return int-the converted integer
*/
int atoi(const char *s)
{
  int value = 0;
  int sign = 1;

  if(*s == ' ') {
      s++;
  }

  if(*s == '-') {
    sign = -1;
    s++;
  }
  else if(*s == '+') {
    s++;  
  }

  while(*s != '\0') {
    if(*s == '.' || *s == ' ' || *s < 48 || *s > 57) {
        return sign * value;
    }
    
    value = value*10 + *s - '0';
    s++;
  }
  return sign * value;
}

/**
  Converts an integer base 10 to an ASCII string
  @param int num- number to be converted
  @param char* str-location to store the string
  @return char*-location of new string
*/
char* itoa(int num, char* str) {
  int isNegative = 0;
  int count = 0;

  if(num == 0) { // TODO [cjgatto]: Very quick fix, consider making better
    str[0] = '0';
  }

  if(num < 0) {
    isNegative = 1;
  }

  while(num != 0 ) {
    int rem = num%10;
    str[count++] = (rem > 9)? (rem - 10) + 'a' : rem + '0';
    num = num/10;
  }

  if(isNegative) {
    str[count++] = '-';
  }

  strrev(str);

  return str;
}

/**
  Reverses a string
  @param char* str- String to be reversed
*/
void strrev(char* str) {
  int size = strlen(str);
  int count;
  char *begin = str;
  char *end = str;
  char temp;

  for(count=0; count<size-1; count++) {
    end++;
  } 

  for(count=0; count<size/2; count++) {
    temp = *end;
    *end = *begin;
    *begin = temp;

    begin++;
    end--;
  }
}


/**
  Compares two strings
  @param char* s1- first string to compare
  @param char* s2- second second string to compare
  @return int- 0 if equal,
*/

int strcmp(const char *s1, const char *s2) {
  while(*s1 && (*s1 == *s2)){
    s1++;
    s2++;
  }

  return ( *(const unsigned char *)s1 - *(const unsigned char *)s2 );
}

/**
  Concatenate the contents of one string onto another.
  @param char* s1-destination
  @param s2-source string
  @return char*- new string
*/
char* strcat(char *s1, const char *s2)
{
  char *rc = s1;
  if (*s1) while(*++s1);
  while( (*s1++ = *s2++) );
  return rc;
}

/**
  Determine if a character is whitespace.
  @param c-character to check
  @return 1 if whitespace 0 if not
*/
int isspace(const char *c)
{
  if (*c == ' '  ||
      *c == '\n' ||
      *c == '\r' ||
      *c == '\f' ||
      *c == '\t' ||
      *c == '\v'){
    return 1;
  }
  return 0;
}

/**
  Set a region of memory.
  @params s-destination, c-byte to write, n-count
*/
void* memset(void *s, int c, size_t n)
{
  unsigned char *p = (unsigned char *) s;
  while(n--){
    *p++ = (unsigned char) c;
  }
  return s;
}

/**
  Procedure..: strtok
  Description..: Split string into tokens
  @Params..: s1-string, s2-delimiter
*/
char* strtok(char *s1, const char *s2)
{
  static char *tok_tmp = NULL;
  const char *p = s2;

  //new string
  if (s1!=NULL){
    tok_tmp = s1;
  }
  //old string cont'd
  else {
    if (tok_tmp==NULL){
      return NULL;
    }
    s1 = tok_tmp;
  }

  //skip leading s2 characters
  while ( *p && *s1 ){
    if (*s1==*p){
      ++s1;
      p = s2;
      continue;
    }
    ++p;
  }

  //no more to parse
  if (!*s1){
    return (tok_tmp = NULL);
  }

  //skip non-s2 characters
  tok_tmp = s1;
  while (*tok_tmp){
    p = s2;
    while (*p){
      if (*tok_tmp==*p++){
	*tok_tmp++ = '\0';
	return s1;
      }
    }
    ++tok_tmp;
  }

  //end of string
  tok_tmp = NULL;
  return s1;
}

/**
  Clears a string by filling it with null characters.
  @param char* str-string to be cleared
  @param int len-length of the string
*/
void strclr(char* str) {
  int count;
  for(count=0; count<strlen(str); count++) {
    str[count] = '\0';
  }
}
