#ifndef _PCB_H
#define _PCB_H

#define READY 1
#define RUNNING 2
#define BLOCKED 3
#define SUSPENDED 4
#define NOT_SUSPENDED 5
#define SYSTEM 1
#define APPLICATION 2 

#include <core/stack.h>
 
// Process Control Block
typedef struct PCB {
  char name[21];
  int clas;
  int priority;
  int rrb;
  int sus;
  unsigned char  stackBase[1024];//unsigned char stackBase[1024];
  unsigned char* stackTop;//unsigned char stackTop[1024];
  struct PCB* next;
  struct PCB* prev;
} PCB;

// Queue
typedef struct QUEUE {
  int count;
  PCB* head;
  PCB* tail;
} QUEUE;

/*******************************************************
 * COMMAND HANDLERS
 *******************************************************/

//**********************
// Permanent Handlers
//**********************

/**
  Changes the state of a PCB to be suspended.
  @return int- Error code if applicable
*/
int handle_suspend();

/**
  Changes the state of a PCB to not besuspended.
  @return int- Error code if applicable: Checks name
*/
int handle_resume();

/**
  Changes the priority of a PCB based on inital input.
  @return int- Error code if applicable: Checks name, priority range
*/
int handle_setpriority();

/**
  Displays the name, class, states, and priority of a PCB.
  @return int- Error code if applicable: Checks name
*/
int handle_showpcb();

/**
  Displays information for each PCB in both queues.
  @return int- Error code if applicable
*/
int handle_showall();

/**
  Displays information for each PCB in the ready queue.
  @return int- Error code if applicable
*/
int handle_showready();

/**
  Displays information for each PCB in the blocked queue.
  @return int- Error code if applicable
*/
int handle_showblocked();

//**********************
// Temporary Handlers
//**********************

/**
  Constructs a PCB based on initial user input.
  @return int- Error code if applicable: Checks name, class, states, and priority
*/
int handle_createpcb();

/**
  Finds and initiates the removal of a PCB.
  @return int- Error code if applicable: Checks name
*/
int handle_deletepcb();

/**
  Changes the state of a PCB to blocked and places it into the appropriate queue.
  @return int- Error code if applicable: Checks name
*/
int handle_block();

/**
  Changes the state of a PCB to unblocked and places it into the appropriate queue.
  @return int- Error code if applicable: Checks name
*/
int handle_unblock();

/**
  Clears both queues of all PCBs. (Might not be temporary)
  @return int- Error or Success code
*/
int handle_clearqueues();

/*******************************************************
 * PROCEDURES
 *******************************************************/

/**
  Uses sys_alloc_mem(...) to allocate memory for a new PCB.
  @return PCB*- Pointer to the PCB, defaults to NULL if faulty
*/
PCB* allocatePCB();

/**
  Uses sys_free_mem(...) to free the memory associated with a PCB.
  @param PCB* pcb- PCB to free
  @return int- Error or Success code
*/
int freePCB(PCB* pcb);

/**
  Uses allocatePCB() and initializes the data of the PCB.
  @param char* name- Name of the PCB
  @param int clas- Class type of the PCB
  @param int priority- Priority of the PCB
  @return PCB*- A pointer to the created PCB.
*/
PCB* setupPCB(char* name, int clas, int priority);

/**
  Searches both queues for the specified PCB.
  @param char* name- Name of a PCB
  @return PCB*- Pointer to the located PCB, NULL if not found
*/
PCB* findPCB(char* name);

/**
  Identifies what queue the PCB belongs in and inserts it accordingly.
  @param PCB* pcb- PCB to insert into a queue
*/
void insertPCB(PCB* pcb);

/**
  Locates and removes the specified PCB.
  @param PCB* pcb- PCB to be removed
  @return int- Success or Error code
*/
int removePCB(PCB* pcb);

/*******************************************************
 * HELPER FUNCTIONS
 *******************************************************/

/**
  Displays meaningful information of the supplied PCB.
  @param PCB* pcb- PCB to be displayed
*/
void displayPCB(PCB* pcb);

/**
  Provides the string representation of the suspention state.
  @param int- Suspension status to convert
  @return char*- Text representation of the suspension status
*/
char* repr_status(int sus);

/**
  Provides the string representation of the ready, running, blocked state.
  @param int- Ready, running, blocked state to convert
  @return char*- Text representation of the state
*/
char* repr_state(int rrb);

/**
  Provides the string representation of the class.
  @param int- System or Application to convert
  @return char*- Text representation of the class
*/
char* repr_clas(int clas);

/**
  Gets the head of the readyqueue
  @param none
  @return PCB*- The head of the readyqueue
*/
PCB* get_ready_proc();

int handle_resumeall();

#endif
