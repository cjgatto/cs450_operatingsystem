#ifndef _VERSION_H
#define _VERSION_H

/**
  Processes and handles the version command.
  @return int - signals the continuation of the loop.
*/
int handle_version(void);

#endif