/*
  Procedure..: getSeconds
  Description..: returns the seconds from clock (BCD)
*/
unsigned char getSeconds();

/*
  Procedure..: getHours
  Description..: returns the hour from clock (BCD)
*/
unsigned char getHours();

/*
  Procedure..: getDayInWeek
  Description..: returns the day of the week from clock (BCD)
*/
unsigned char getDayInWeek();

/*
  Procedure..: getDayInMonth
  Description..: returns the day of the month from clock (BCD)
*/
unsigned char getDayInMonth();

/*
  Procedure..: getMonth
  Description..: returns the month from clock (BCD)
*/
unsigned char getMonth();

/*
  Procedure..: getYear
  Description..: returns the year from clock (BCD)
*/
unsigned char getYear();

int power(int base,int exponent);


int bcdToI(unsigned char byte);


void iToBCD(int num,unsigned char* byte);

/*
  Procedure..: getMinutes
  Description..: returns the Minutes from clock (BCD)
*/
unsigned char getMinutes();

/*
  Procedure..: setDate
  Description..: Changes the clock time
  Params..: int hr-new hour
	..: int min -new mins
	..: int sec -new seconds
*/
void setTime(int hr,int min, int sec);

/*
  Procedure..: setDate
  Description..: Changes the clock date
  Params..: int month-new month
	..: int day -new day
	..: int year -new year
*/
void setDate(int month, int day, int year);

/*
  Procedure..: handle_getdate
  Description..: executes the getdate command
*/
int handle_getdate();


/*
  Procedure..: handle_setdate
  Description..: gets the input for setdate and executes command
*/
int handle_setdate();

/*
  Procedure..: handle_getTime
  Description..: executes getTime
*/
int handle_gettime();

/*
 Procedure..: handle_settime
  Description..: gets the input for settime and executes command
*/
int handle_settime();
