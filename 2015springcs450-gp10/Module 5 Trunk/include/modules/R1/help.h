#ifndef _HELP_H
#define _HELP_H

/**
  Processes and handles the help command.
  @return int - signals the continuation of the loop.
*/
int handle_help(void);

#endif