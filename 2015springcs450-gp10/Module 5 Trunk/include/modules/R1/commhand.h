#ifndef _COMMHAND_H
#define _COMMHAND_H

#define BREAK 0
#define CONTINUE 1
#define KILL 2

typedef struct COMMAND {
	char* text;
	int(*funct)(void);
	char* help;
} COMMAND;

#define COMM_COUNT 29
COMMAND commands[COMM_COUNT];
#define SHUTDOWN 0
#define VERSION 1
#define HELP 2
#define GET_DATE 3
#define SET_DATE 4
#define GET_TIME 5
#define SET_TIME 6
#define SUSPEND 7
#define RESUME 8 
#define SET_PRIORITY 9
#define SHOW_PCB 10
#define SHOW_ALL 11
#define SHOW_READY 12
#define SHOW_BLOCKED 13
#define CREATE_PCB 14
#define DELETE_PCB 15
#define BLOCK 16
#define UNBLOCK 17
#define CLEARQUEUES 18
#define YIELD 19
#define LOADR3 20
#define RESUMEALL 21
#define SHOW_ALLOCATED 22
#define SHOW_FREE 23
#define IS_EMPTY 24
#define SHOW_HEAP 25
#define INIT_HEAP 26
#define FREE_MCP 27
#define ALLOCATED_MCB 28

/**
  Prints some basic input upon startup.
*/
void general_info(void);

/**
  Initializes the main program loop, prompting for user input.
*/
void init_commhand(void);

int handle_loadr3(void);

int handle_yield(void);

#endif
