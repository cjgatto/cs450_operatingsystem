#ifndef _STACK_H
#define _STACK_H

#define STACK_SIZE 5

typedef struct Stack Stack;

void init_stack(Stack *stack);

char* top_stack(Stack *stack);

void push_stack(Stack *stack, char *command);

void pop_stack(Stack *stack);

#endif