var searchData=
[
  ['limit',['limit',['../structidt__struct.html#a68fd3b4f6c14a331ca9b226cbf122e13',1,'idt_struct::limit()'],['../structgdt__descriptor__struct.html#a68fd3b4f6c14a331ca9b226cbf122e13',1,'gdt_descriptor_struct::limit()'],['../tables_8h.html#a68fd3b4f6c14a331ca9b226cbf122e13',1,'limit():&#160;tables.h']]],
  ['limit_5flow',['limit_low',['../structgdt__entry__struct.html#af9013229edfb91d4820f66b8df890ce3',1,'gdt_entry_struct::limit_low()'],['../tables_8h.html#af9013229edfb91d4820f66b8df890ce3',1,'limit_low():&#160;tables.h']]],
  ['load_5fpage_5fdir',['load_page_dir',['../paging_8h.html#a3affceba4cd194e1c516404c14abbe7c',1,'load_page_dir(page_dir *new_page_dir):&#160;paging.c'],['../paging_8c.html#a31e6c585cbda542534f1b0fc83e40689',1,'load_page_dir(page_dir *new_dir):&#160;paging.c']]],
  ['loader',['loader',['../dispatcher_8h.html#a223b995baa0aa1ddc384fe26e1e9c1f1',1,'loader(char *name, void(*func)(void)):&#160;dispatcher.c'],['../dispatcher_8c.html#a223b995baa0aa1ddc384fe26e1e9c1f1',1,'loader(char *name, void(*func)(void)):&#160;dispatcher.c']]],
  ['loadr3',['LOADR3',['../commhand_8h.html#a4aa727b2d5f87b82dba676dad620c4ca',1,'commhand.h']]]
];
