/*
  ----- commhand.c -----

  Description..: Command Handler for user interface.
  Uses a UNIX style Command Line Interface.
*/
#include <modules/R1/commhand.h>
#include <system.h>
#include <core/serial.h>
#include <string.h>
#include <core/stack.h>
#include <core/dispatcher.h>
#include <modules/R1/shutdown.h>
#include <modules/R1/version.h>
#include <modules/R1/help.h>
#include <modules/R1/date.h>
#include <modules/R2/pcb.h>
#include <modules/R3/procsr3.h>
#include "modules/mpx_supt.h"

/**
  Initializes command
*/
COMMAND initCommand(char* text, int (*funct)(void), char help[MAX_BUFF]) {
  COMMAND c;
	c.text = text;
	c.funct = funct;
	c.help = help;
  return c;
}

/**
  Initializes the main program loop, prompting for user input.
*/
void init_commhand(void) {  
  commands[SHUTDOWN] = initCommand("shutdown", &handle_shutdown, "Murex -> Shutdown:\nThe 'shutdown' command is used to end all Murex activites and power down.\nConfirmation is required to shutdown."); 
  commands[VERSION] = initCommand("version", &handle_version, "Murex -> Version:\nThe 'version' command display's the system's current build.");
  commands[HELP] = initCommand("help", &handle_help, "Murex -> Help:\nThe 'help' command displays useful data regarding how to use other system commands.\nType 'Help {command}' to recieve help text.");
  commands[GET_DATE] = initCommand("getdate", &handle_getdate, "Murex -> getdate:\nThe 'getdate' command retrieve's the current system date.\nThe date is returned in the form of mm/dd/yy.");
  commands[SET_DATE] = initCommand("setdate", &handle_setdate, "Murex -> setdate:\nThe 'setdate' command sets the current system date.\nThe date must be supplied in the form mm/dd/yy.\nUpon completion, the date is returned in the form of mm/dd/yy.");
  commands[GET_TIME] = initCommand("gettime", &handle_gettime, "Murex -> gettime:\nThe 'gettime' command sets the current system time.\nThe time is then returned in the form hh:mm:ss.");
  commands[SET_TIME] = initCommand("settime", &handle_settime, "Murex -> settime:\nThe 'settime' command sets the current system time.\nThe time must be supplied in the form hh:mm:ss.\nUpon completion, the time is returned in the form of hh:mm:ss.");
  commands[SUSPEND] = initCommand("suspend", &handle_suspend, "Murex -> suspend:\nThe 'suspend' command changes the suspensionstate of the PCB to suspended.\nThe parameter must be a valid PCB name.");
  commands[RESUME] = initCommand("resume", &handle_resume, "Murex -> resume:\nThe 'resume' command changes the suspensionstate of the PCB to not suspended.\nThe parameter must be a valid PCB name.");
  commands[SET_PRIORITY] = initCommand("setpriority", &handle_setpriority, "Murex -> setpriority:\nThe 'setpriority' command changes the priority of the PCB.\nThe parameters must be a valid PCB name, and a valid priority.");
  commands[SHOW_PCB] = initCommand("showpcb", &handle_showpcb, "Murex -> showpcb:\nThe 'showpcb' command displays meaningful information for the desired PCB.\nThe parameter must be a valid PCB name.");
  commands[SHOW_ALL] = initCommand("showall", &handle_showall, "Murex -> showall:\nThe 'showall' command displays all PCBs in the system.");
  commands[SHOW_READY] = initCommand("showready", &handle_showready, "Murex -> showready:\nThe 'showready' command shows all PCBs in the ready queue.");
  commands[SHOW_BLOCKED] = initCommand("showblocked", &handle_showblocked, "Murex -> showblocked:\nThe 'showblocked' command shows all PCBs in the blocked queue.");
  commands[CREATE_PCB] = initCommand("createpcb", &handle_createpcb, "Murex -> createpcb [TEMP]:\nThe 'createpcb' command creates a PCB.\nThe parameters must be a valid PCB name, class, and priority.\nThis is a temporary command.\n\tName: Must not exceed 20 characters in length.\n\tClass: 1 (System), 2 (Application).\n\tPriority: 0-9\nExample:\n\tMurexample> createpcb brandNewPcb 2 5");
  commands[DELETE_PCB] = initCommand("deletepcb", &handle_deletepcb, "Murex -> deletepcb [TEMP]:\nThe 'deletepcb' command deletes a PCB.\nThe parameter must be a valid PCB name.\nThis is a temporary command.");
  commands[BLOCK] = initCommand("block", &handle_block, "Murex -> block [TEMP]:\nThe 'block' command changes the state of a PCB to blocked.\nThe parameter must be a valid PCB name.\nThis is a temporary command.");
  commands[UNBLOCK] = initCommand("unblock", &handle_unblock, "Murex -> unblock [TEMP]:\nThe 'unblock' command changes the state of a PCB to ready.\nThe parameter must be a valid PCB name.\nThis is a temporary command.");
  commands[CLEARQUEUES] = initCommand("clearqueues", &handle_clearqueues, "Murex -> clearqueue [TEMP]:\nThe 'clearqueue' command removes all PCB's from the queue.\nThere are no parameters for this command.");
  commands[YIELD] = initCommand("yield", &handle_yield, "Murex -> yield [TEMP]:\nThe 'yield' command yields control of the CPU to another process other than commhand.");
  commands[LOADR3] = initCommand("loadr3", &handle_loadr3, "Murex -> loadr3 [TEMP]:\nThe 'loadr3' command simply loads tests for R3.");
	commands[RESUMEALL] = initCommand("resumeall", &handle_resumeall, "Murex -> resumeall [TEMP]:\nThe 'resumeall' command sets all PCBs to 'NOT_SUSPENDED'");
  
  general_info();

  int cont = 1;
  char input[MAX_BUFF+1];

  while(cont) {
    serial_print(PROMPT);
    serial_pollln(input, MAX_BUFF);
    serial_println("");

    char *command = strtok(input, " ");
	
  	int i;
  	for(i = 0; i < COMM_COUNT; i++) {
  		if(strcmp(command, commands[i].text) == 0) {
        cont = commands[i].funct(); 
        break;
      }
  	}
  	if(i == COMM_COUNT && command != NULL) {
      serial_print("-murex: ");
      serial_print(command);
      serial_println(": Command not found.");	
  	}
      input[0]='\0';
      sys_req(IDLE);	
  }
  sys_req(EXIT);	
}

/**
  Prints some basic input upon startup. And ASCII art!
*/
void general_info() {
  char int_str[50];
  serial_println("\n\n");
  serial_println("\t                      -:.                            ");
  serial_println("\t                    :mNNN+                           ");
  serial_println("\t                   +mmmNmh`                          ");
  serial_println("\t              .:+oo+-``.:hmh+:`          `           ");
  serial_println("\t     //+o:/+sso/-:::://+:osooyhoo/-.`-:odm/          ");
  serial_println("\t      Nm+ysdhoyo/....:o+hhdh-:./:+odNNMNNMs--        ");
  serial_println("\t    omm:``...ohhdy+//``odNmd..:-``  `-+mMNmo.        ");
  serial_println("\t     dd`````.:-.:-```  `:-+hs+/-````  `odd/          ");
  serial_println("\t      -```.--:.  `.--...--``:++s:..`.shmm-`          ");
  serial_println("\t    +y````.-:``-.-.-...-+-.````/hys++:yshy`          ");
  serial_println("\t    y+.:sds:```/.`.`.-```.--`-/`:Nmddy:.odo`         ");
  serial_println("\t   `dy+oy/-. `.-.---:.`.``:o+-.``hNy:.` :yd-         ");
  serial_println("\t  ++h+s...`: `-:.`---..-.``/````:ds/.``` /ds`        ");
  serial_println("\t+mNy-.`````/.`.:-`````:-``--``````yo:``-..hmy:`      ");
  serial_println("\t.NNNys`````.-``...:+..-..://--::-ymys-/m:`/NNNNdhh/  ");
  serial_println("\t  `:oh+.````No.-`......::.-/:.``-mmddsoNo//NMMMMNy`  ");
  serial_println("\t     `+y+-.-o:``..--y/-.```.`.-smmmdNysMmNmNMNMNs    ");
  serial_println("\t      `.+y+:o.``````:o:````..yyyhyyoyysyys/+/:``     ");
  serial_println("\t   ``````.-:+.`.......-//-...oooo+o++++++++/:-.``    ");
  serial_println("\t`````````...///-++:---::/sdyyy+++++++///:::--..```   ");
  serial_println("\t  ``````....:mdoo+++++++///::::::----........```     ");
  serial_println("\t     ``````..:::--........--........````````````     ");
  serial_println("\t           ___________________________ ");
  serial_println("\t          |                           |");
  serial_println("\t          |   Murex Command Line OS   |");
  serial_println("\t          |___________________________|\n");
  serial_print  ("\t           Input Buffer Size: ");
  itoa(MAX_BUFF, int_str);
  serial_println(int_str);
  serial_print  ("\t           Input History Size: ");
  itoa(HISTORY_LIMIT, int_str);
  serial_println(int_str);
  serial_println("");
}

/**
  Handles the 'yield' function that allows Murex to run other processes.
  @return int- Return indicates for the loop to continue.
*/
int handle_yield(void) {
	yield();
	return CONTINUE;
}

/**
  Handles the 'loadr3' function that loads all r3 processes into memory.
  @return int- Return indicates for the loop to continue.
*/
int handle_loadr3(void) {
	insertPCB(loader("proc1", &proc1));
	serial_println("Proc1: Loaded");
	insertPCB(loader("proc2", &proc2));
	serial_println("Proc2: Loaded");
	insertPCB(loader("proc3", &proc3));
	serial_println("Proc3: Loaded");
	insertPCB(loader("proc4", &proc4));
	serial_println("Proc4: Loaded");
	insertPCB(loader("proc5", &proc5));
	serial_println("Proc5: Loaded");
	return CONTINUE;
}
