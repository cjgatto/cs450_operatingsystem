#ifndef _DISPATCHER_H
#define _DISPATCHER_H

#include <modules/R2/pcb.h>

PCB* loader(char* name, void (*func)(void));

void yield(void);

void insertIdle();

#endif
