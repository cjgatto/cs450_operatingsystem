/*
  ----- kmain.c -----

  Description..: Kernel main. The first function called after
      the bootloader. Initialization of hardware, system
      structures, devices, and initial processes happens here.
*/
#include <stdint.h>
#include <string.h>
#include <system.h>
#include <core/io.h>
#include <core/serial.h>
#include <core/tables.h>
#include <core/interrupts.h>
#include <modules/R1/commhand.h>
#include <mem/heap.h>
#include <mem/paging.h>
#include "modules/mpx_supt.h"
#include <core/dispatcher.h>
#include <modules/R2/pcb.h>

void kmain(void)
{
  extern uint32_t magic;
  // Uncomment if you want to access the multiboot header
  // extern void *mbd;
  // char *boot_loader_name = (char*)((long*)mbd)[16];
  // 0) Initialize Serial I/O and call mpx_init

  klogv("Starting MPX boot sequence...");
  klogv("Initialized serial I/O on COM1 device...");
	init_serial(0x3f8); // COM1 = 0x3f8
	set_serial_out(0x3f8);
	set_serial_in(0x3f8);
	// mpx_init(MODULE_R1);
  	// mpx_init(MODULE_R2);
	// mpx_init(MODULE_R3);
	mpx_init(MODULE_R4);

  // 1) Check that the boot was successful and correct when using grub
  // Comment this when booting the kernel directly using QEMU, etc.
  if ( magic != 0x2BADB002 ){
    //kpanic("Boot was not error free. Halting.");
  }
   
  // 2) Descriptor Tables
  klogv("Initializing descriptor tables...");
  init_gdt();
  init_idt();

  // 3) Program Interrupt Controller
  klogv("Initializing program interrupt controller...");
  init_pic();
  init_irq();
  // enable interrupts sti()...?

  // 4) Virtual Memory
  klogv("Initializing virtual memory...");
  init_paging();

  // 5) Call Commhand
  klogv("Transferring control to commhand...");
  // init_commhand();
  PCB* comm = loader("commhand", &init_commhand);
  comm -> priority = 9;
  comm -> sus = NOT_SUSPENDED;
  insertPCB(comm);
  insertIdle();
  yield();

  // 11) System Shutdown
  klogv("Starting system shutdown procedure...");
   
  /* Shutdown Procedure */
  klogv("Shutdown complete. You may now turn off the machine. (QEMU: C-a x)");
  hlt();
}
