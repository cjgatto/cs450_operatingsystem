/*
  ----- serial.c -----

  Description..: Contains methods and variables used for
    serial input and output.
*/

#include <stdint.h>
#include <string.h>
#include <system.h>

#include <core/io.h>
#include <core/serial.h>

#define NO_ERROR 0

//Command History
static char input_history[HISTORY_LIMIT][MAX_BUFF];
static int history_location = 0;
static int current_location = 0;

// Active devices used for serial I/O
int serial_port_out = 0;
int serial_port_in = 0;

/**
  Initializes the serial port.
  @params device - Port to connect to
*/
int init_serial(int device)
{
  outb(device + 1, 0x00); //disable interrupts
  outb(device + 3, 0x80); //set line control register
  outb(device + 0, 115200/9600); //set bsd least sig bit
  outb(device + 1, 0x00); //brd most significant bit
  outb(device + 3, 0x03); //lock divisor; 8bits, no parity, one stop
  outb(device + 2, 0xC7); //enable fifo, clear, 14byte threshold
  outb(device + 4, 0x0B); //enable interrupts, rts/dsr set
  (void)inb(device);      //read bit to reset port
  return NO_ERROR;
}

/**
  Prints a message to output, followed by a new line.
  @params msg - The text to be printed
  @return int - Successful or not
*/
int serial_println(const char *msg)
{
  int i;
  for(i=0; *(i+msg)!='\0'; i++){
    outb(serial_port_out,*(i+msg));
  }
  outb(serial_port_out,'\r');
  outb(serial_port_out,'\n');  
  return NO_ERROR;
}

/**
  Prints a message to output.
  @params msg - The text to be printed
  @return int - Successful or not
*/
int serial_print(const char *msg)
{
  int i;
  for(i=0; *(i+msg)!='\0'; i++){
    outb(serial_port_out,*(i+msg));
  }
  if (*msg == '\r') outb(serial_port_out,'\n');
  return NO_ERROR;
}

/**
  Serial_polln handles all keyboard input. The gathered string is stored in the 'line' parameter.
  @params line[] - The string to store user input
  @params MAX_LENGTH - Maximum number of input characters
*/
void serial_pollln(char line[MAX_BUFF], int MAX_LENGTH) {
  int length = 0; // Length of the input
  int cursor = 0; // Cursor's current position          
  int historyfirst=0;
  char tmp_str[2];
  tmp_str[1] = '\0';

  while(1) {
    if(inb(COM1 + 5) & 1) {
      char in_char = inb(COM1);
      tmp_str[0] = in_char;

      if(in_char == ESCAPE) {

        in_char = inb(COM1);
        in_char = inb(COM1);

        if(in_char == 'A') { //Up arrow
          if(historyfirst&&(current_location==history_location)){
            //NOOP
          }
          else if(*input_history[get_prev_location()]){
            historyfirst=1;
            current_location = get_prev_location();
            line = strcpy(line, input_history[current_location]);
            int i;
            for(i = cursor; i < length; i++) {
              serial_print(" ");
            }
            for(i = length; i > 0; i--) {
              backspace_sequence();
            }
            if(line != NULL) {
              serial_print(line);
            }
            length = strlen(line);
            cursor = length;
          }
        }
        else if(in_char == 'B') { // Down arrow
          if((history_location == current_location) || (current_location == history_location-1)) {
            int i;
            for(i = cursor; i < length; i++) {
              serial_print(" ");
            }
            for(i = length; i > 0; i--) {
              backspace_sequence();
            }
            line[0] = '\0';
            length = 0;
            cursor = length;
          }
          else if(*input_history[advance_location()]){
            historyfirst=1;
            current_location = advance_location();
            line = strcpy(line, input_history[current_location]);
            int i;
            for(i = cursor; i < length; i++) {
              serial_print(" ");
            }
            for(i = length; i > 0; i--) {
              backspace_sequence();
            }
            if(line != NULL) {
              serial_print(line);
            }
            length = strlen(line);
            cursor = length;
          }
        }
        else if(in_char == 'C') { // Right arrow
          if(cursor < length) {
            tmp_str[0] = line[cursor];
            serial_print(tmp_str);
            cursor++;
          }
        }   
        else if(in_char == 'D') { // Left arrow
          if(cursor > 0) {
            serial_print("\b");
            cursor--;
          }
        }
        else {
          in_char = inb(COM1);
          int inc;
          if(in_char == '~' && cursor != length) {    // Delete
            for(inc = cursor; inc < length; inc++) {
              line[inc] = line[inc+1];
              tmp_str[0] = line[inc+1];
            }
            line[inc-1] = '\0';
            for(inc = cursor; inc <= length; inc++) {
              serial_print(" ");
            }
            for(inc = length; inc >= 0; inc--) {
              backspace_sequence();
            }
            serial_print(line);
            length--;
            for(inc = cursor; inc < length; inc++) {
              serial_print("\b");
            }
          }
        }
      }
      else if(in_char == BACKSPACE) { //Backspace from end of line
        if(cursor == length && length != 0) {
          backspace_sequence();
          length--;
          cursor--;
        }
        else if(cursor > 0) {
          int inc;
          serial_print("\b");
          for(inc = cursor; inc < length; inc++) {
            line[inc-1] = line[inc];
            tmp_str[0] = line[inc-1];
            serial_print(tmp_str);
          }
          serial_print(" ");
          for(inc = cursor; inc <= length; inc++) {
            serial_print("\b");
          }
          length--;
          cursor--;
        }
      }
      else if(in_char == '\n' || in_char == '\r') { // End of input
        line[length] = '\0';
        if(strcmp(line, "") != 0) {
          strcpy(input_history[history_location], line);
          history_location = get_next_location();
        }
        current_location = history_location;
        historyfirst=0;
        break;
      }
      else if(length < MAX_LENGTH) { // Normal character
        if(cursor == length) { // Write character to end of string and prompt
          line[cursor] = in_char;
          serial_print(tmp_str);
        }
        else { // Write character within string and prompt
          int inc;
          char temp;
          char cursor_char = in_char;

          for(inc = cursor; inc <= length; inc++) {
            temp = line[inc];
            line[inc] = cursor_char;
            tmp_str[0] = cursor_char;
            serial_print(tmp_str);
            cursor_char = temp;
          }
          for(inc = cursor; inc < length; inc++) {
            serial_print("\b");
          }
        }
        length++;
        cursor++;
      }
    }
  }
}

/**
  Executes one full backspace sequence to the ouput. 
*/
void backspace_sequence(void) {
  serial_print("\b");
  serial_print(" ");
  serial_print("\b");
}

/**
  Supplies the next location index when searching through input history. 
  @returns int - The next usable index.
*/
int get_prev_location(void) {
  if(current_location == 0) {
    return HISTORY_LIMIT - 1;
  }
  return current_location - 1;
}

/**
  Supplies the next location index when storing input history. 
  @returns int - The next usable index.
*/
int advance_location(void) {
  if(current_location + 1 == HISTORY_LIMIT) {
    return 0;
  }
  return current_location + 1;
}

/**
  Supplies the next location index when storing input history. 
  @returns int - The next usable index.
*/
int get_next_location(void) {
  if(history_location + 1 == HISTORY_LIMIT) {
    return 0;
  }
  return history_location + 1;
}

/**
  Sets serial_port_out to the given device address.
  All serial output, such as that from serial_println, will be
  directed to this device. 
  @params device - desired device
  @returns int - Successful or not
*/
int set_serial_out(int device) {
  serial_port_out = device;
  return NO_ERROR;
}

/**
  Sets serial_port_in to the given device address.
  All serial input, such as console input via a virtual machine,
  QEMU/Bochs/etc, will be directed to this device. 
  @params device - desired device
  @returns int - Successful or not
*/
int set_serial_in(int device) {
  serial_port_in = device;
  return NO_ERROR;
}
