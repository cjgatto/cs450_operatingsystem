/*
 * file_main.h
 *
 *  Created on: Apr 13, 2015
 *      Author: chrisgatto
 */

#ifndef FILE_MAIN_H_
#define FILE_MAIN_H_

#include <stdio.h>

#define CONTINUE 1
#define ERROR -1
#define MAX_BUFF 256
#define ucstar unsigned char *

typedef struct COMMAND {
	char* text;
	int(*funct)(void);
	char* help;
} COMMAND;

typedef struct directoryEntry {
	char fileName [9];
	char ext [4];
	int attribute;
	int firstCluster;
	int fileSize;
} directoryEntry;

typedef struct bootInfo {
	int bytesPerSector;
	int sectorsPerCluster;
	int numberOfReservedClusters;
	int maxRootEntries;
	int numberOfFatCopies;
	int totalNumberOfSectors;
	int numberOfSectorsPerFat;
	int sectorsPerTrack;
	int numberOfHeads;
	int totalSectorCountForFAT32;
	int signatureValue;
	int volumeID;
	char volumeLabel[12];
	char fileSystemType[9];
} bootInfo;

#define COMM_COUNT 6
COMMAND commands[COMM_COUNT];
#define PRINT_BSI 0
#define PRINT_ROOT_DIR 1
#define CHANGE_DIRECTORY 2
#define LIST_DIRECTORY 3
#define TYPE 4
#define RENAME 5

int main(int argc,char * args[]);
void initialize_commands(void);
void initialize_commands(void);
void initialize_bootInfo(void);
void initialize_fatTable(void);
COMMAND initCommand(char* text, int (*funct)(void), char help[MAX_BUFF]);
int handle_type(void);
int handle_rename(void);
int handle_print_bsi(void);
int handle_list_directory(void);
int handle_print_root_dir(void);
int handle_change_directory(void);
int change_directory(char* comm);
int convertToInt(unsigned char* bytes,int numberOfBytes);
int readInt(int numberOfBytes);
void writeInt(int number,int numberOfBytes);
void toSector(int sector);
void printDir(directoryEntry* c, int num);
directoryEntry* findDirectory(char* dir);
char *trim(char *str);
void populateDirectory( directoryEntry * directory,int numberOfEntries,int startSector);
int getIndex(char* name,char* ext);
int getIndexFromIndex(char* name, char* ext, int start);
void printEntry(directoryEntry entry);
void dump(char* name,char* ext);
int numberOfSectors(int startingSector);
void toDataSector(int sector);
void type(char* name,char* ext);
void add_spaces(char *dest, int num_of_spaces);

#endif /* FILE_MAIN_H_ */
