#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "file_main.h"

FILE* fpointer;
int fatTable [3072];
bootInfo boot;
directoryEntry root[224];
directoryEntry* curr;
int sizeOfCurr, startOfCurr;

/**
  Main runner for this file management system. Initializes commands, boot
  sector, fat table, and command line.
  @param int argc - Number of program arguments
  @param char * args[] - List of program arguments
  @return int - Success code decribing completion
*/
int main(int argc,char * args[]) {
	if(argc < 2) { // Fist arg is current path
		printf("Must give file name as first argument.\n");
		return ERROR;
	}

	fpointer = fopen(args[1],"r+");

	initialize_commands();
	initialize_bootInfo();
	initialize_fatTable();

	int i,j;
	for(i=0;i<14;i++) {
		toSector(19+i);
		for(j=0;j<16;j++) {
			fread(root[16*i+j].fileName,1,8,fpointer);
			fread(root[16*i+j].ext,1,3,fpointer);
			root[16*i+j].attribute=readInt(1);
			fseek(fpointer,14,SEEK_CUR);
			root[16*i+j].firstCluster=readInt(2);
			root[16*i+j].fileSize=readInt(4);
		}
	}

	curr = root;
	sizeOfCurr = 272;
	startOfCurr = 14;

	if(!fpointer) {
		printf("File not found\n");
		return ERROR;
	}

	if(argc == 2) {
		int cont = CONTINUE;
		char input[MAX_BUFF+1];

		while(cont == CONTINUE) {
			printf("> ");
			scanf(" %[^\n]",input);
			char* command = strtok(input, " ");

			int i;
			for(i = 0; i < COMM_COUNT; i++) {
				if(strcmp(command, commands[i].text) == 0) {
					cont = commands[i].funct();
					break;
				}
			}
			if(i == COMM_COUNT && command != NULL) {
				printf("<main> Command not found\n");
			}
			input[0] = '\0';
		}
	}
	else if(argc == 3) {
		char* path = args[2];
		char* file;
		char* ext;

		if(strstr(path, "/") != NULL) {
			char* subdir = strtok(path, "/");
			file = strtok(NULL, ".");
			change_directory(subdir);
		}
		else {
			file = strtok(path, ".");
		}
		ext = strtok(NULL, "");
		dump(file, ext);
	}
	else {
		printf("Error: Invalid number of arguments\n");
	}
}

/**
  Initializes all user commands.
*/
void initialize_commands(void) {
	commands[PRINT_BSI] = initCommand("bsi", &handle_print_bsi, "Print boot sector information");
	commands[PRINT_ROOT_DIR] = initCommand("rd", &handle_print_root_dir, "Print root directory");
	commands[CHANGE_DIRECTORY] = initCommand("cd", &handle_change_directory, "Change directory");
	commands[LIST_DIRECTORY] = initCommand("ls", &handle_list_directory, "List directory");
	commands[TYPE] = initCommand("type", &handle_type, "Show type");
	commands[RENAME] = initCommand("rename", &handle_rename, "Rename a file");
}

/**
  Loads boot sector information into a global structure.
*/
void initialize_bootInfo(void) {
	fseek(fpointer,11,SEEK_SET);
	boot.bytesPerSector = readInt(2);
	boot.sectorsPerCluster = readInt(1);
	boot.numberOfReservedClusters = readInt(2);
	boot.numberOfFatCopies = readInt(1);
	boot.maxRootEntries = readInt(2);
	boot.totalNumberOfSectors = readInt(2);
	fseek(fpointer,22,SEEK_SET);
	boot.numberOfSectorsPerFat = readInt(2);
	boot.sectorsPerTrack = readInt(2);
	boot.numberOfHeads = readInt(2);
	fseek(fpointer,32,SEEK_SET);
	boot.totalSectorCountForFAT32 = readInt(4);
	fseek(fpointer,38,SEEK_SET);
	boot.signatureValue = readInt(1);
	boot.volumeID = readInt(4);
	fread(boot.volumeLabel,1,11,fpointer);
	fread(boot.fileSystemType,1,8,fpointer);
}

/**
  Initializes fat table into a global structure
*/
void initialize_fatTable(void) {
	int i;
	toSector(1);
	for(i=0; i<3072;i+=2){
		unsigned char temp[3];
		fread(temp,1,3,fpointer);
		fatTable[i] = ((temp[1] & 0x0f) << 8) + temp[0];
		fatTable[i+1] = ((temp[2] & 0xf0) << 4) + ((temp[2] & 0x0f) << 4) + ((temp[1] & 0xf0) >> 4);
	}
}

/**
  Creates a COMMAND used by the command line to process user input
  @param char* text - Text used to invoke the command
  @param int (*funct)(void) - pointer to the respective handling function
  @param char help[MAX_BUFF] - Command related help text
  @return COMMAND - Initialized COMMAND structure
*/
COMMAND initCommand(char* text, int (*funct)(void), char help[MAX_BUFF]) {
	COMMAND c;
	c.text = text;
	c.funct = funct;
	c.help = help;
	return c;
}

/**
  Handles user input for the "type" command.
  @return int- Error or Success code
*/
int handle_type(void) {
	char* comm = strtok(NULL, " ");

	if(comm == NULL) {
		printf("Must include file.\n");
		return CONTINUE;
	}

	char* name = strtok(comm, ".");
	char* ext = strtok(NULL, " ");

	type(name, ext);

	return CONTINUE;
}

/**
  Handles the user command for printing the boot sector information
  @return int- Error or Success code
*/
int handle_print_bsi(void) {
	printf("\tSectors per cluster: %d\n", boot.sectorsPerCluster);
	printf("\tNumber of Reserved Clusters: %d\n", boot.numberOfReservedClusters);
	printf("\tMax Root Entries: %d\n", boot.maxRootEntries);
	printf("\tNumber of Fat Copies: %d\n", boot.numberOfFatCopies);
	printf("\tTotal Number of Sectors: %d\n", boot.totalNumberOfSectors);
	printf("\tNumber of Secotrs per Fat: %d\n", boot.numberOfSectorsPerFat);
	printf("\tSectors per Track: %d\n", boot.sectorsPerTrack);
	printf("\tNumber of Heads: %d\n", boot.numberOfHeads);
	printf("\tTotal Sector Count for Fat 32: %d\n", boot.totalSectorCountForFAT32);
	printf("\tSignature Value: %d\n", boot.signatureValue);
	printf("\tVolume ID: %d\n", boot.volumeID);
	printf("\tValue Lable: %s\n", boot.volumeLabel);
	printf("\tFile System Type: %s\n", boot.fileSystemType);
	printf("\n");
	return CONTINUE;
}

/**
  Handles the user command for renaming a file
  @return int- Error or Success code
*/
int handle_rename(void) {
	char* file1 = strtok(NULL, " ");
	char* file2 = strtok(NULL, " ");

	if(!file1 || !file2) {
		printf("Must include two file names: file to rename, and new name.\n");
		return CONTINUE;
	}

	char* name1 = strtok(file1, ".");
	char* ext1 = strtok(NULL, "");
	if(!ext1) {
		ext1 = "   ";
	}

	int index = getIndex(name1, ext1);

	char* name2 = strtok(file2, ".");
	char* ext2 = strtok(NULL, "");
	if(!ext2) {
		ext2 = "   ";
	}

	if(!name1 || !name2 || index == -1 || strlen(name2) > 8 || strlen(ext2) > 3) {
		printf("Invalid file names.\n");
		return CONTINUE;
	}
	else if(ext2 != "   " && strcasecmp(trim(ext1), trim(ext2)) != 0) {
		printf("Cannot change the file extention.\n");
		return CONTINUE;
	}

	int i;
	int sector = index/16;
	int indexInSector = index - 16*sector;

	if(curr == root) {
		toSector(19+sector);
	}
	else {
		int currentSector = startOfCurr;
		for(i=0; i<sector; i++){
			currentSector = fatTable[currentSector];
		}
		toDataSector(currentSector);
	}

	fseek(fpointer, 32*indexInSector, SEEK_CUR);
	fwrite(name2, 1, 8, fpointer);
	fwrite(ext2,1,3,fpointer);

	for(i=0; i<8; i++) {
		if(i < strlen(name2)) {
			curr[index].fileName[i] = name2[i];
		}
		else {
			curr[index].fileName[i] = ' ';
		}
	}

	for(i=0; i<3; i++) {
		if(i < strlen(ext2)) {
			curr[index].ext[i] = ext2[i];
		}
		else {
			curr[index].ext[i] = ' ';
		}
	}

	if(curr == root) {
		toSector(14);
	}
	else {
		toDataSector(startOfCurr);
	}
	return CONTINUE;
}

/**
  Adds a specified number of spaces to a string
  @param char *dest - String to alter
  @param int num_of_space - number of spaces to add
*/
//void add_spaces(char *dest, int num_of_spaces) {
//	sprintf(dest, "%s%*s", dest, num_of_spaces, "");
//}

/**
  Handles the user command for printing the root directory
  @return int - Error or Success code
*/
int handle_print_root_dir(void) {
	printDir(root, 272);
	return CONTINUE;
}

/**
  Handles the user command for changing directories
  @return int- Error or Success code
*/
int handle_change_directory(void) {
	char* comm = strtok(NULL, " ");
	change_directory(comm);
	return CONTINUE;
}

/**
  Changes the current directory to eiter a sub directory, or the root directory
  @param char* comm - Contains information to determine destination directory
  @return int- Error or Success code
*/
int change_directory(char* comm) {
	if(comm == NULL) {
		if(curr != root) {
			free(curr);
		}
		toSector(19);
		curr = root;
		sizeOfCurr = 272;
		startOfCurr = 14;
	}
	else {
		int index = getIndex(comm, "");

		if(index != -1) {
			int sectors = numberOfSectors(curr[index].firstCluster);
			int numOfEntries = sectors * 16;

			directoryEntry *entries = malloc(sizeof(directoryEntry) * numOfEntries);
			int currentSector = curr[index].firstCluster;
			toDataSector(currentSector);
			directoryEntry* temp;
			populateDirectory(entries, numOfEntries, currentSector);

			startOfCurr = curr[index].firstCluster;

			if(curr != root) {
				free(curr);
			}
			curr = entries;
			sizeOfCurr = numOfEntries;
		}
		else {
			printf("Sub-directory %s not found.\n", comm);
		}
	}
	return CONTINUE;
}

/**
  Handles the user command for displaying the files within a directory
  @return int- Error or Success code
*/
int handle_list_directory(void) {
	char* comm = strtok(NULL, " ");

	if(comm == NULL) {
		printDir(curr, sizeOfCurr);
	}
	else {
		char* name = strtok(comm, ".");
		char* ext = strtok(NULL, "");
		int matches[10];
		int index;
		int count = 0;

		index = getIndex(name, ext);
		while(index != -1) {
			matches[count++] = index;
			index = getIndexFromIndex(name, ext, index+1);
		}

		for(index=0; index<count; index++) {
			printEntry(curr[matches[index]]);
		}
	}
	return CONTINUE;
}

/**
  Formats and prints the given directory's information
  @param directoryEntry* c - Directory to describe
  @param int num - Number of entries in the directory
*/
void printDir(directoryEntry* c, int num) {
	int i;
	printf("File Name\tCluster\t\tSize\n");
	for(i = 0; i < num; i++) {
		if((unsigned char)  c[i].fileName[0] == 0x00) {
			break;
		}
		else if((unsigned char) c[i].fileName[0] == 0xE5 ||
				((unsigned int) c[i].attribute & 0x02) == 0x02)  {
			continue;
		}
		printEntry(c[i]);
	}
}

/**
  Formats and prints a single directory entry
  @param directoryEntry entry - entry to print
*/
void printEntry(directoryEntry entry) {
	if((entry.attribute & 0x02) != 0x02) {
		printf("%s", trim(entry.fileName));
		if(strcmp(entry.ext, "   ") == 0) {
			printf("    ");
		}
		else {
			printf(".%s", entry.ext);
		}
		int i = 12 - (strlen(entry.fileName) + strlen(entry.ext));
		for(; i>=0; i--) {
			printf(" ");
		}
		printf("\t%d\t\t%d\n", entry.firstCluster, entry.fileSize);
	}
}

/**
  Converts a number of bytes to an integer
  @param unsigned char* bytes - List of bytes to convert
  @param int numberOfBytes - number of bytes to convert
  @return int - converted integer
*/
int convertToInt(unsigned char* bytes,int numberOfBytes) {
	int i;
	int j = 0;
	for(i=0; i<numberOfBytes; i++){
		j+=((bytes[i] & 0xff) << (8*i));
	}
	return j;
}

/**
  Reads an integer from the current file pointer location
  @param int numberOfBytes - number of bytes to read from the file
  @return int - integer read from file
*/
int readInt(int numberOfBytes) {
	unsigned char temp[numberOfBytes];
	fread(temp,1,numberOfBytes,fpointer);
	return convertToInt(temp,numberOfBytes);
}

/**
  Writes an integer to the current file pointer location
  @param int number - number to write to the file
  @param int numberOfBytes - number of bytes to write to the file
*/
void writeInt(int number,int numberOfBytes) {
	unsigned char temp[numberOfBytes];
	int i;
	for(i=0; i<numberOfBytes; i++){
		temp[i]=((number & (0xff << 8*i)) >> (8*i));
	}
	fwrite(temp, 1, numberOfBytes, fpointer);
}

/**
  Moves the file pointer to the desired sector, from the beginning of
  the file pointer
  @param int sector - desired sector to point to
*/
void toSector(int sector) {
	fseek(fpointer,512*sector,SEEK_SET);
}

/**
  Returns a duplicated string with no leading or trailing whitespace
  @param char *str - String to duplicate
  @return char * - String with no leading or trailing whitespace
*/
char *trim(char *str) {
	size_t len = 0;
	char *frontp = str;
	char *endp = NULL;

	if( str == NULL ) { return NULL; }
	if( str[0] == '\0' ) { return str; }

	len = strlen(str);
	endp = str + len;

	while( isspace(*frontp) ) { ++frontp; }
	if( endp != frontp ) {
		while( isspace(*(--endp)) && endp != frontp ) {}
	}

	if( str + len - 1 != endp )
		*(endp + 1) = '\0';
	else if( frontp != str &&  endp == frontp )
		*str = '\0';

	endp = str;
	if( frontp != str ) {
		while( *frontp ) { *endp++ = *frontp++; }
		*endp = '\0';
	}
	return str;
}

/**
  Creates directory entries to populate a given directory
  @param directoryEntry* directory - directory to populate with entries
  @param int numberOfEntries - Number of entries the directory contains
  @param int startSector - The starting sector of the given directory
*/
void populateDirectory(directoryEntry* directory, int numberOfEntries, int startSector) {
	int i, currentSector;
	currentSector = startSector;
	toSector(31+startSector);
	for(i=0; i<numberOfEntries; i++) {
		if(i != 0 && i%512 == 0) {
			currentSector = fatTable[currentSector];
			toSector(31+currentSector);
		}
		fread(directory[i].fileName,1,8,fpointer);
		fread(directory[i].ext,1,3,fpointer);
		directory[i].attribute=readInt(1);
		fseek(fpointer, 14, SEEK_CUR);
		directory[i].firstCluster = readInt(2);
		directory[i].fileSize = readInt(4);
	}
}

/**
  Finds the index of a directory entry within the current directory
  @param char* name - name of the entry's filename
  @param char* ext - extention of the entry's file extention
  @return int - Index of the located entry, otherwise -1
*/
int getIndex(char* name, char* ext) {
	return getIndexFromIndex(name, ext, 0);
}

/**
  Finds the index of a directory entry within the current directory. Allows controlling where
  the search starts from in order to locate multiple matches
  @param char* name - name of the entry's filename
  @param char* ext - extention of the entry's file extention
  @param int start - What index to start searching from
  @return int - Index of the located entry, otherwise -1
*/
int getIndexFromIndex(char* name, char* ext, int start) {
	int i;

	if(ext == NULL || ext == "   ") {
		ext = "";
	}

	for(i=start;i<sizeOfCurr;i++){
		if(((unsigned char)curr[i].fileName[0])==0xE5) {
			continue;
		}
		else if(((unsigned char)curr[i].fileName[0])==0x00) {
			break;
		}
		else if(((strcmp(name, "*") == 0) || (strcasecmp(trim(curr[i].fileName),name)==0)) &&
				((strcmp(ext, "*") == 0) || (strcasecmp(trim(curr[i].ext),ext)==0))) {
			return i;
		}
	}
	return -1;
}

/**
  Moves the file pointer to the desired sector adjusted to point to the data sector
  @param int sector - desired sector to point to
*/
void toDataSector(int sector) {
	toSector(31+sector);
}

/**
  Return the number of sectors between the given sector and the final sector
  @param int startingSector - sector to start counting from
*/
int numberOfSectors(int startingSector) {
	int currentSector = startingSector;
	int i = 1;

	while(1) {
		if(((currentSector >= 0xFF8) && (currentSector <= 0xfff)) || // Last cluster in file
				(fatTable[currentSector] == 0xff7) || // Next cluster is bad
				(fatTable[currentSector] == 0x00)) { // Next cluster is unused
			break;
		}
		i++;
		currentSector = fatTable[currentSector];

	}
	return i;
}

/**
  Prints all data from the select file to standard out
  @param char* name - name of the file to dump
  @param char* ext - extention of the file to dump
*/
void dump(char* name,char* ext) {
	unsigned char buff[512];
	int i = getIndex(name,ext);
	int j = 0;
	int k = 0;

	if(i == -1) {
		printf("File not found in current directory.\n");
		return;
	}
	int currentSector=curr[i].firstCluster;
	toDataSector(currentSector);
	int sectors=numberOfSectors(currentSector);

	while (k < sectors && j < curr[i].fileSize) {
		if((curr[i].fileSize-j) > 512) {
			fread(buff, 1, 512, fpointer);
			fwrite(buff, 1, 512, stdout);
			j += 512;
		}
		else {
			fread(buff,1,curr[i].fileSize-j,fpointer);
			fwrite(buff,1, curr[i].fileSize-j, stdout);
			break;
		}
		currentSector = fatTable[currentSector];
		toDataSector(currentSector);
		k++;
	}
	printf("\n");
}

/**
  Prints all data from the select file. Only available for txt, bat, and c files.
  @param char* name - name of the file to type
  @param char* ext - extention of the file to type
*/
void type(char* name,char* ext) {
	if(ext == NULL) {
		ext = "";
	}

	if(!(strcasecmp(ext,"TXT")==0 || strcasecmp(ext,"BAT")==0 || strcasecmp(trim(ext),"C") == 0)) {
		printf("can only type files with an extension of C, TXT, or BAT.\n");
		return;
	}
	char buff[513];
	buff[512]='\0';
	int i = getIndex(name,ext);
	int j = 0;
	int k = 0;

	if(i == -1){
		printf("File not found in current directory.\n");
		return;
	}
	int currentSector=curr[i].firstCluster;
	toDataSector(currentSector);
	int sectors=numberOfSectors(currentSector);
	while (k < sectors && j < curr[i].fileSize) {
		if((curr[i].fileSize-j) > 512){
			fread(buff,1,512,fpointer);
			j+=512;
			printf(buff);
			getchar();
		}
		else {
			fread(buff,1,curr[i].fileSize-j,fpointer);
			buff[curr[i].fileSize-j]='\0';
			printf(buff);
			break;
		}
		currentSector = fatTable[currentSector];
		toDataSector(currentSector);
		k++;
	}
	printf("\n");
}
